import React,{Component} from 'react';
import {View,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import FastImage from 'react-native-fast-image'






 class AdversmentCard extends Component {
    
    render(){
        const {data,onPress} = this.props;
        
        return(
            <View
            style={{elevation:10, marginVertical:moderateScale(2), alignSelf:'center', backgroundColor:'white', width:responsiveWidth(98),height:responsiveHeight(27)}}
            >

            <TouchableOpacity
             onPress={()=>{
                if(onPress){
                   onPress();
                }
            }}
             style={{flex:1,}} >
                <FastImage
                resizeMode='cover'
                source={{uri:data.img[0]}}
                style={{alignSelf:'center',marginVertical:moderateScale(3),width:responsiveWidth(95),height:responsiveHeight(25),borderRadius:moderateScale(1.5)}}
                />
            </TouchableOpacity>


            
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
   
});

export default connect(mapStateToProps)(AdversmentCard);
