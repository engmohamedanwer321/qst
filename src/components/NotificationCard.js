import React,{Component} from 'react';
import {View,Animated,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppText from '../common/AppText';
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'

const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);




class NotificationCard extends Component {
    
    state = {
        ground: 0,
        fadeAnim: new Animated.Value(0)
    }
    
    componentDidMount(){
        const {index} = this.props
        Animated.timing(                  
            this.state.fadeAnim,            
            {toValue: 1,duration:5000,delay:0}
          ).start();    
        moment.locale(this.props.isRTL ? 'ar' : 'en');
    }

    readNotification = (notID) => {
          console.log(notID)
        axios.put(`${BASE_END_POINT}notif/${notID}/read`,{}, {
            headers: {
              'Content-Type': 'application/json',
              //this.props.currentUser.token
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
        }).then(Response=>{
            console.log("notification is red")
            console.log(Response)
            this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
        }).catch(error=>{
            console.log(error.response)
        })
    }

    render(){
        const {noti,isRTL,navigator} = this.props;
        //const dateToFormat = () => <Moment date={date} />,
        return(
            <MyTouchableOpacity style={{
                alignSelf:'center',
             height:responsiveHeight(13) ,  
            width: responsiveWidth(98),
            backgroundColor:noti.read||this.state.ground>0? 'rgba(250,250,250,0.8)' : '#E8ECFF',
            //elevation:2,
            //shadowOffset:{width:1,height:2},
            }}
            onPress={()=>{
                this.setState({ground:1});
                this.readNotification(noti.id)
                if(noti.description.toLowerCase().includes('accept your rigister')){
                    navigator.push({
                        screen: 'NotificationDetails',
                        animated: true,
                        passProps:{
                            data: noti,
                            img: require('../assets/imgs/active.png'),
                            title: Strings.congrats
                        }
                    })
                    
                }else if(noti.description.toLowerCase().includes('active your account')){
                    navigator.push({
                        screen: 'NotificationDetails',
                        animated: true,
                        passProps:{
                            data: noti,
                            img: require('../assets/imgs/active2.png'),
                            title: Strings.congrats
                        }
                    })
                    
                    
                }else if(noti.description.toLowerCase().includes('refuse')){
                    navigator.push({
                        screen: 'NotificationDetailsRefusedOrder',
                        animated: true,
                        passProps:{
                            data: noti,
                        }
                    })
                    
                }else if(noti.description.toLowerCase().includes('accept your order')){
                    navigator.push({
                        screen: 'NotificationDetails',
                        animated: true,
                        passProps:{
                            data: noti,
                            img: require('../assets/imgs/acceptOrder.png'),
                            title: Strings.acceptOrder,
                            go:true
                        }
                    })
                    
                }else if(noti.description.toLowerCase().includes('your order on progress')){
                    navigator.push({
                        screen: 'NotificationDetails',
                        animated: true,
                        passProps:{
                            data: noti,
                            img: require('../assets/imgs/onprogress.png'),
                            title: Strings.onProgress,
                            //go:true
                        }
                    })
                    //paymentRemember.png
                }else if(noti.description.toLowerCase().includes('remember to pay')){
                    navigator.push({
                        screen: 'NotificationDetails',
                        animated: true,
                        passProps:{
                            data: noti,
                            img: require('../assets/imgs/paymentRemember.png'),
                            title: Strings.onProgress,
                            go:true
                        }
                    })
                    //paymentRemember.png
                }else if(noti.description.toLowerCase().includes('your salesman has been changed')){
                    navigator.push({
                        screen: 'NotificationDetails',
                        animated: true,
                        passProps:{
                            data: noti,
                            img: require('../assets/imgs/change.png'),
                            title: Strings.changeSalesMan,
                            go:true
                        }
                    })
                    //paymentRemember.png
                }else{
                    navigator.push({
                        screen: 'NotificationDetails',
                        animated: true,
                        passProps:{
                            data: noti,
                            img: require('../assets/imgs/notificationAlert.png'),
                            title: Strings.qstMsg,
                            //go:true
                        }
                    })
                }
                      
            }}
            >
               <View style={{alignItems:'center',marginHorizontal:moderateScale(6), marginTop:moderateScale(4), flexDirection:isRTL?'row-reverse':'row'}}>
                    <Thumbnail style={{borderWidth:1}} small source={require('../assets/imgs/mtqst.png')} />
                    <View style={{marginHorizontal:moderateScale(5)}}>
                        <AppText text={noti.arabicDescription.length>=30?noti.arabicDescription.substring(0, 25)+"...":noti.arabicDescription} fontSize={responsiveFontSize(7)} color={colors.buttonColor} />
                    </View>
               </View>
               
               <View style={{marginHorizontal:moderateScale(5), alignSelf:isRTL?'flex-start':'flex-end'}}>
                    <AppText text={moment(noti.createdAt).fromNow()} color='gray' />
               </View>

            </MyTouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(NotificationCard);
