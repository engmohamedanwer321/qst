import React,{Component} from 'react';
import {View,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import withPreventDoubleClick from './withPreventDoubleClick';
import {getCurrentProduct} from '../actions/ProductAction';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);


 class ProfileProductCard extends Component {
     state={
         basketColor:null,
         added: false,
     }
     
    render(){
        const {data,deleteFunProduct,isRTL,navigator} = this.props;
        return(
            <View 
            style={{alignSelf:'center', backgroundColor:'#ededed', border:1, width:responsiveWidth(98),height:responsiveHeight(16)}}
            >
                <View style={{alignItems:'center', marginTop:moderateScale(5), width:responsiveWidth(90),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between'}}>
                    <AppText text={`- ${data.name}`} color={colors.buttonColor} fontSize={responsiveFontSize(2.5)} fontWeight='400' />
                    <AppText text={moment('2018-12-15T17:57:46.299Z').fromNow()} color='gray' />
                </View>
                <View style={{marginHorizontal:moderateScale(6), marginTop:moderateScale(5), alignSelf:isRTL?'flex-end':'flex-start',flexDirection:isRTL?'row-reverse':'row'}}>
                    <Button
                    onPress={()=>{
                        this.props.getCurrentProduct(data);
                        navigator.push({
                            screen:'UpdateProduct',
                            animated: true,
                        })
                    }}
                     style={{borderColor:'transparent', width:responsiveWidth(20),justifyContent:'center',alignItems:'center',height:responsiveHeight(4),borderRadius:moderateScale(10),backgroundColor:colors.buttonColor,borderWidth:1}}>
                        <AppText text={Strings.edit} color='white' />
                    </Button>
                    <Button
                    onPress={()=>{
                        if(deleteFunProduct){
                            deleteFunProduct();
                        }
                    }}
                     style={{borderColor:'transparent', marginHorizontal:moderateScale(2), width:responsiveWidth(20),justifyContent:'center',alignItems:'center',height:responsiveHeight(4),borderRadius:moderateScale(10),backgroundColor:'red',borderWidth:1}}>
                        <AppText text={Strings.delete} color='white' />
                    </Button>
                </View>
                <View style={{marginTop:moderateScale(7), backgroundColor:'#707070',height:1, alignSelf:'center',width:responsiveWidth(85)}} />
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
});

const mapDispatchToProps = {
    getCurrentProduct,
}

export default connect(mapStateToProps,mapDispatchToProps)(ProfileProductCard);
