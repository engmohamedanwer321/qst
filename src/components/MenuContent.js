import React, { Component } from 'react';
import {
  View,
  Text,Alert,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Button, Content, Icon,Thumbnail } from 'native-base';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/MenuActions';
import logout from "../actions/LogoutActions";
import { AppText, MenuItem } from '../common';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import { rootNavigator } from '../screens/Login';
import Strings from '../assets/strings';
import {selectMenu} from '../actions/MenuActions';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';




class MenuContent extends Component {

  state = {
    v: false,
  }

  render() {
    const {item,currentUser,color} = this.props;
    if(this.props.isRTL){
      Strings.setLanguage('ar')
    }else{
      Strings.setLanguage('en')
    }
    console.log("menu Content item =>   "+item);
    return (
      <ScrollView style={{flex: 1,backgroundColor: color,width: responsiveWidth(82),}}>
        {currentUser&&
        <View style={{marginTop:hp(5), alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
          <AppText fontWeight='500' text={`${currentUser.user.firstname} ${currentUser.user.lastname}`} fontSize={wp(5.5)} />
          <AppText fontWeight='500' text={`${currentUser.user.phone[0]}`} fontSize={wp(5)} />
        </View>
        }

        <View style={{marginTop:hp(5), alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
          <Thumbnail
           style={{borderColor:colors.buttonColor}} large
            source={currentUser&&currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/profileicon.png')} />
        </View> 

             
        <View 
        showsVerticalScrollIndicator={false}
        style={{marginTop:hp(4)}}>
          <View style={{ marginBottom: hp(3)}}>
          <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item[item.length-1] == "HOME"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('HOME');
                  this.props.navigator.push({
                    screen:'Home',
                    animated: true
                  })
                }
              }}
             focused={item[item.length-1] == 'HOME'}  iconName='home' text={Strings.home}
             />
            
              {currentUser&&currentUser.user.type=='SALES-MAN'&&
             <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item[item.length-1] == "MYCLIENTS"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('MYCLIENTS');
                  this.props.navigator.push({
                    screen:'SalesmanHome',
                    animated: true
                  })
                }
              }}
             focused={item[item.length-1] == 'MYCLIENTS'} iconName='users' text={Strings.myClients}
             />
            }
            
              <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item[item.length-1] == "OFFERS"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  //
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('OFFERS');
                  this.props.navigator.push({
                    screen:'Offers',
                    animated: true
                  })
                }
              }}
             focused={item[item.length-1] == 'OFFERS'} iconName='award' text={Strings.offers}
             />
            
              
              <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item[item.length-1] == "CATEGORIES"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('CATEGORIES');
                  this.props.navigator.push({
                    screen:'Categories',
                    animated: true
                  })
                }
              }}
             focused={item[item.length-1] == 'CATEGORIES'} iconName='tshirt' text={Strings.categories}
             />

              <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item[item.length-1] == "ADDS"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('ADDS');
                  this.props.navigator.push({
                    screen:'Adds',
                    animated: true
                  })
                }
              }}
             focused={item[item.length-1] == 'ADDS'} iconName='ad' text={Strings.adds}
             />
             
             {currentUser&&
             <MenuItem
             onPress={()=>{
               console.log("main 1 =>   "+item);
               if(item[item.length-1] == "MYORDERS"){
                 this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
               }else{
                 this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                 this.props.selectMenu('MYORDERS');
                 this.props.navigator.push({
                   screen:'MyOrders',
                   animated: true
                 })
               }
             }}
            focused={item[item.length-1] == 'MYORDERS'} iconName='poll-h' text={Strings.myOrders}
            />
            }


            {currentUser&&
            <MenuItem
            onPress={()=>{
              console.log("main 1 =>   "+item);
              if(item[item.length-1] == "NOTIFICATION"){
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              }else{
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('NOTIFICATION');
                this.props.navigator.push({
                  screen:'Notifications',
                  animated: true
                })
              }
            }}
           focused={item[item.length-1] == 'NOTIFICATION'} iconName='bell' text={Strings.notifications}
           />
           }

            {!currentUser&&
            <MenuItem
            onPress={()=>{
              console.log("main 1 =>   "+item);
              if(item[item.length-1] == "LOGIN"){
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              }else{
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('LOGIN');
                this.props.navigator.push({
                  screen:'Login',
                  animated: true
                })
              }
            }}
           focused={item[item.length-1] == 'LOGIN'} iconName='sign-in-alt' text={Strings.login}
           />
            }

            {currentUser&&
            <MenuItem
            onPress={()=>{
            if(this.props.currentUser){
               if(item[item.length-1]=="PROFILE"){
                 this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
               }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                 this.props.selectMenu('PROFILE');
                 this.props.navigator.push({
                   screen:'Profile',
                   animated:true,
                 })
               }
            }else{
             RNToasty.Warn({title:Strings.checkUser})
            }
             
           }}
           focused={item[item.length-1] == 'PROFILE'}
           iconName='user' text={Strings.profile}/>
            }

             
            <MenuItem
             onPress={()=>{
              if(item[item.length-1]=="SETTINGS"){
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              }else{
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('SETTINGS');
                this.props.navigator.push({
                  screen:'SelectLanguage',
                  animated:true,
                })
              }
              
            }}
            focused={item[item.length-1] == 'SETTINGS'}
             iconName='cog' text={Strings.settings}/>

          
            

            <MenuItem
            onPress={()=>{
              if(item[item.length-1]=="ABOUT"){
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              }else{
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('ABOUT');
                this.props.navigator.push({
                  screen: 'About',
                  animated: true
                }); 
              }
              }}
            focused={item[item.length-1] == 'ABOUT'}
             iconName='info-circle' text={Strings.aboutUS}/>

             {currentUser&&
             <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item[item.length-1] == "CHAT"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('CHAT');
                  this.props.navigator.push({
                    screen:'ChatPeople',
                    animated: true
                  })
                }
              }}
             focused={item[item.length-1] == 'CHAT'} iconName='comment-alt' text={Strings.chat}
             />
            }

          {currentUser &&
           <Button 
           onPress={()=>{
             this.props.logout(this.props.userToken,this.props.currentUser.token,this.props.navigator)
           }}
           style={{marginTop:hp(5), backgroundColor:'white', alignSelf:'center',width:wp(50),justifyContent:'center',alignItems:'center',borderRadius:wp(3) }}>
                <AppText text={Strings.logout} color={colors.darkPrimaryColor} fontSize={wp(5)}/>
           </Button>
          }

          </View>
        </View>
       
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: colors.darkPrimaryColor,
    width: responsiveWidth(82),
    

  },

  linksContainer: {
    flex: 1,
    marginBottom: moderateScale(15),
  },
  linksContainerScroll: {
    flex: 1,
    marginTop: moderateScale(50)
  },

};

const mapStateToProps = state => ({
  item: state.menu.item,
  isRTL: state.lang.RTL,
  currentUser : state.auth.currentUser,
  userToken: state.auth.userToken,
  color: state.lang.color,

});

const mapDispatchToProps = {
  selectMenu,
  logout,
}

export default connect(mapStateToProps, mapDispatchToProps, )(MenuContent);
