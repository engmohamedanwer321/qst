import React,{Component} from 'react';
import {View,TouchableOpacity,Text,Alert} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';
import {Button,Icon,Thumbnail} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {AddProductToBacket} from '../actions/OrderAction';

import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class ClientCard extends Component {
     state={
        puy:false
     } 

    render(){
        const {data,onPress,isRTL,navigator} = this.props;
        console.log('data')
        console.log(data)
        return(
            <MyButton 
            onPress={()=>{
                if(onPress){
                    onPress()
                }
            }}
            style={{flexDirection:'column', justifyContent:'center',alignItems:'center', marginVertical:moderateScale(5),marginHorizontal:moderateScale(2), alignSelf:'center', backgroundColor:'transparent', width:responsiveWidth(48),height:responsiveHeight(25)}}
            >

                <Thumbnail large source={data.img?{uri:data.img}:require('../assets/imgs/profileicon.png')} />
                <Text style={{marginVertical:moderateScale(7)}} > 
                    {data.firstname }  {data.lastname }
                </Text>
            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    AddProductToBacket,
}

export default connect(mapStateToProps, mapDispatchToProps)(ClientCard);
