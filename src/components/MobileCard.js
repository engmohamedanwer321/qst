import React,{Component} from 'react';
import {View,TouchableOpacity,Text,Alert} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import {AddProductToBacket} from '../actions/OrderAction';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class MobileCard extends Component {
     state={
        puy:false,
        pricePerMonth:0,
        firstMonth:0,
     } 

     componentDidMount(){
        const {data} = this.props;
        let p = data.hasOffer ? data.offer : data.installmentPrice;
        let rem = p % 12;
        let newPrice =  p - rem;
        let costPerMonth = newPrice / 12;
        let firstMont = rem+costPerMonth;
        this.setState({
            firstMonth:firstMont,
            pricePerMonth:costPerMonth
        })
     }

    render(){
        const {data,onPress,isRTL,navigator} = this.props;
        
        return(
            <MyButton 
            onPress={()=>{
                navigator.push({
                   screen: 'ProductDetails',
                   passProps:{
                       product:data,
                   }
               })
            }}
            style={{ marginVertical:moderateScale(5),marginHorizontal:moderateScale(2), alignSelf:'center', backgroundColor:'transparent', width:responsiveWidth(50),height:responsiveHeight(49)}}
            >

             <View style={{width:responsiveWidth(50),height:responsiveHeight(49)}}>
                <FastImage 
                source={{uri:data.img[0]}}
                style={{ borderRadius:moderateScale(1), marginVertical:moderateScale(3),marginTop:moderateScale(8), alignSelf:'center', width:responsiveWidth(40),height:responsiveHeight(27)}}
                />

                <Text style={{alignSelf:'center', color:colors.darkPrimaryColor,fontSize:responsiveFontSize(7),fontStyle:'italic' }} >{data.name}</Text>
                {/* #f0f215 */}
                <Text style={{marginTop:moderateScale(2),alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'#ff1c8e',fontSize:responsiveFontSize(7),fontStyle:'italic' }} >{data.quantity} {strings.pices}</Text>
                
                <View style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between'}}>
                    <View style={{}}>
                    <Text style={{textDecorationLine:data.hasOffer?'line-through':'none',alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:data.hasOffer?'red':colors.darkPrimaryColor,fontSize:responsiveFontSize(7),fontStyle:'italic',marginVertical:moderateScale(1.5) }} >{data.installmentPrice} {strings.pe}</Text>
                     {data.hasOffer&&
                     <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:colors.darkPrimaryColor,fontSize:responsiveFontSize(6),fontStyle:'italic' }} >{data.offer} {strings.pe}</Text>
                     }
                    </View>
                    <TouchableOpacity
                      onPress={()=>{
                        this.setState({puy:true})
                        this.props.AddProductToBacket({
                            product:data.id,
                            count:1,
                            paymentSystem:"installment",
                            firstPaid:this.state.firstMonth,
                            monthCount:12,
                            costPerMonth:this.state.pricePerMonth
                        },data)
                        RNToasty.Success({title:Strings.addProductSuccessfuly})
                    }}
                    >
                        <Icon name="cart" type="MaterialCommunityIcons" style={{color:this.state.puy?'green':'gray' }} />
                    </TouchableOpacity>
                </View>
             </View>
           
            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    AddProductToBacket,
}

export default connect(mapStateToProps,mapDispatchToProps)(MobileCard);
