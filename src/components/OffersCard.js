import React,{Component} from 'react';
import {View,TouchableOpacity,Text,Alert} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {AddProductToBacket} from '../actions/OrderAction';

import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class OffersCard extends Component {
     state={
        puy:false,
        pricePerMonth:0,
        firstMonth:0,
     } 

     componentDidMount(){
        const {data} = this.props;
        let rem = data.offer % 12;
        let newPrice =  data.offer - rem;
        let costPerMonth = newPrice / 12;
        let firstMont = rem+costPerMonth;
        this.setState({
            firstMonth:firstMont,
            pricePerMonth:costPerMonth
        })
     }
 

    render(){
        const {data,onPress,isRTL,navigator} = this.props;
        console.log('data')
        console.log(data)
        return(
            <MyButton 
            onPress={()=>{
                navigator.push({
                   screen: 'ProductDetails',
                   passProps:{
                       product:data,
                       offer:true,
                   }
               })
            }}
            style={{ marginVertical:moderateScale(5),marginHorizontal:moderateScale(2), alignSelf:'center', backgroundColor:'transparent', width:responsiveWidth(48),height:responsiveHeight(36)}}
            >

             <View style={{flex:1}}>
                <FastImage 
                source={{uri:data.img[0]}}
                style={{ borderRadius:moderateScale(1), marginVertical:moderateScale(3), alignSelf:'center', width:responsiveWidth(40),height:responsiveHeight(18)}}
                />

                <Text style={{alignSelf:'center', color:'#0c246b',fontSize:responsiveFontSize(6.5),fontStyle:'italic' }} >{data.name}</Text>
                {/* #f0f215 */}
                <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'#ff1c8e',fontSize:responsiveFontSize(6),fontStyle:'italic' }} >{data.quantity} {strings.pices}</Text>
                
                <View style={{flex:1,flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>
                     <View>
                     <Text style={{textDecorationLine: 'line-through',alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'red',fontSize:responsiveFontSize(6),fontStyle:'italic',marginVertical:moderateScale(1.5) }} >{data.installmentPrice} {strings.pe}</Text>
                     <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'#0c246b',fontSize:responsiveFontSize(6),fontStyle:'italic' }} >{data.offer} {strings.pe}</Text>
                     </View>
                    <TouchableOpacity
                    onPress={()=>{
                        this.setState({puy:true})
                        this.props.AddProductToBacket({
                            product:data.id,
                            count:1,
                            paymentSystem:"installment",
                            firstPaid:this.state.firstMonth,
                            monthCount:12,
                            costPerMonth:this.state.pricePerMonth
                        },data)
                        RNToasty.Success({title:Strings.addProductSuccessfuly})
                    }}
                    >
                        <Icon name="cart" type="MaterialCommunityIcons" style={{color:this.state.puy?'green':'gray' }} />
                    </TouchableOpacity>
                </View>
             </View>
           
            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    AddProductToBacket,
}


export default connect(mapStateToProps, mapDispatchToProps)(OffersCard);
