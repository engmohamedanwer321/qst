import React,{Component} from 'react';
import {View,StyleSheet,Alert} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import FastImage from 'react-native-fast-image'
import {Button} from 'native-base';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


export default class CompanyCard extends Component {
    render(){
        const {image,onPress} = this.props;
        return(
            <MyButton
            onPress={()=>{
                if(onPress){
                    onPress()
                }
            }}
              style={styles.card}>
               <FastImage  style={styles.img} source={{uri:image}} />
            </MyButton>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        justifyContent:'center',
        alignItems:'center',
        height:responsiveHeight(14),
        width: responsiveWidth(23),
        borderRadius: moderateScale(45),
        backgroundColor:'white',
        zIndex:10000,
        elevation:2,
        shadowOffset:{width:1,height:2},
        marginHorizontal:moderateScale(5),
    },
    img: {
        width: responsiveWidth(18),
        height: responsiveHeight(11),
        borderRadius: moderateScale(15),
    }
})