import React,{Component} from 'react';
import {View,ImageBackground,Text,Alert,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class AdsCard extends Component {
     state={
         basketColor:null,
         added: false,
     }

     makeProductSponserd = () => {
        console.log("my product  "+this.props.data.product.id)
        axios.put(`${BASE_END_POINT}products/${this.props.data.product.id}/sponsered`, null, {
            headers: {
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
        .then(response=>{
            console.log('product sponserd')
            console.log(response.data);       
            this.setState({uploadProduct:false})
            RNToasty.Success({title:Strings.prodectSponserd})               
        }).catch(error=>{
            console.log(error);
            console.log(error.message);
            console.log(error.status);
            console.log(error.response);
            this.setState({uploadProduct:false})
        })      
        
     }

    render(){
        const {ads,onPress,isRTL,navigator} = this.props;
        
        return(
            <View 
            style={{marginVertical:moderateScale(2), alignSelf:'center', backgroundColor:'white', width:responsiveWidth(99),height:responsiveHeight(25)}}
            >

             <Swiper
             autoplay
             showsButtons={false}
             showsPagination={false}
             >
                 {ads.map(adversment=>(
                     <TouchableOpacity
                     style={{flex:1}}
                     onPress={()=>{
                        navigator.push({
                            screen:'AddsDetails',
                            animated:true,
                            passProps: {
                                data:adversment
                            }
                    })
                    }}
                     >
                     <FastImage 
                     resizeMode='cover'
                     source={{uri:adversment.img[0]}}
                     style={{ borderRadius:moderateScale(1), marginVertical:moderateScale(0), alignSelf:'center', width:responsiveWidth(99),height:responsiveHeight(25)}}
                     />
                     </TouchableOpacity>
                 ))}
                 
             </Swiper>
            
           
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

export default connect(mapStateToProps)(AdsCard);
