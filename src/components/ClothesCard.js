import React,{Component} from 'react';
import {View,TouchableOpacity,Text,Alert} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class ClothesCard extends Component {
     state={
         basketColor:null,
         added: false,
     }

     makeProductSponserd = () => {
        console.log("my product  "+this.props.data.product.id)
        axios.put(`${BASE_END_POINT}products/${this.props.data.product.id}/sponsered`, null, {
            headers: {
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
        .then(response=>{
            console.log('product sponserd')
            console.log(response.data);       
            this.setState({uploadProduct:false})
            RNToasty.Success({title:Strings.prodectSponserd})               
        }).catch(error=>{
            console.log(error);
            console.log(error.message);
            console.log(error.status);
            console.log(error.response);
            this.setState({uploadProduct:false})
        })      
        
     }

    render(){
        const {data,onPress,isRTL} = this.props;
        
        return(
            <MyButton 
            onPress={()=>{
               /* if(onPress){
                   onPress();
                }*/
                Alert.alert("ddd")
            }}
            style={{ marginVertical:moderateScale(5),marginHorizontal:moderateScale(2), alignSelf:'center', backgroundColor:'transparent', width:responsiveWidth(50),height:responsiveHeight(33)}}
            >

             <View style={{flex:1}}>
                <FastImage 
                source={{uri:'https://www.3in1design.com/wp-content/uploads/2016/05/blank_tshirt.jpg'}}
                style={{ borderRadius:moderateScale(1), marginVertical:moderateScale(3), alignSelf:'center', width:responsiveWidth(40),height:responsiveHeight(18)}}
                />

                <Text style={{alignSelf:'center', color:colors.darkPrimaryColor,fontSize:responsiveFontSize(2.2),fontStyle:'italic' }} >CAR car</Text>
                {/* #f0f215 */}
                <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'#f0f215',fontSize:responsiveFontSize(2),fontStyle:'italic' }} >5 {strings.pices}</Text>
                
                <View style={{flex:1,flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>
                     <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:colors.darkPrimaryColor,fontSize:responsiveFontSize(2.5),fontStyle:'italic' }} >5 {strings.pe}</Text>
                    <TouchableOpacity>
                        <Icon name="cart" type="MaterialCommunityIcons" style={{color:'gray' }} />
                    </TouchableOpacity>
                </View>
             </View>
           
            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

export default connect(mapStateToProps)(ClothesCard);
