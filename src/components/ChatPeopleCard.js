import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import {Icon} from 'native-base'
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppText from '../common/AppText';
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
//import { SwipeItem, SwipeButtonsContainer } from 'react-native-swipe-item';
import Swipeout from 'react-native-swipeout';
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);




class ChatPeopleCard extends Component {
     swipeoutBtns = [
        {
          text: 'Button'
        }
      ]
    state = {
        ground: 0,
    }
    
    componentDidMount(){
        moment.locale(this.props.isRTL ? 'ar' : 'en');
    }

    

   

    render(){
        const {data,currentUser,onPress} = this.props;
        //const dateToFormat = () => <Moment date={date} />,
        return(
            <TouchableOpacity 
            onPress={onPress}
            style={{borderBottomColor:'#DEDEDE',borderBottomWidth:0.4, justifyContent:'center', height:responsiveHeight(12),width:responsiveWidth(100)}}>
                <View style={{alignItems:'center', marginHorizontal:moderateScale(5),flexDirection:'row'}}>
                    <Thumbnail small source={{uri:data.salesMan.img}} />
                    <View style={{justifyContent:'center',alignItems:'center', marginHorizontal:moderateScale(3)}}>
                    <Text >{this.props.currentUser.user.type=='CLIENT'?data.salesMan.firstname+" "+data.salesMan.lastname : data.client.firstname+" "+data.client.lastname}</Text>             
                    <Text style={{fontSize:10,color:'gray'}}>{this.props.currentUser.user.type=='CLIENT'?data.salesMan.type : data.client.type}</Text>             
                    </View>

                </View>
            </TouchableOpacity>      
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(ChatPeopleCard);
