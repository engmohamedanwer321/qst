import React,{Component} from 'react';
import {View,TouchableOpacity,Text,Alert} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {AddProductToBacket} from '../actions/OrderAction';

import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class CategoryCard extends Component {
     state={
        puy:false
     } 

    render(){
        const {data,onPress,isRTL,navigator,barColor} = this.props;
        console.log('data')
        console.log(data)
        return(
            <MyButton 
            onPress={()=>{
                if(onPress){
                    onPress()
                }
            }}
            style={{ marginVertical:moderateScale(5),marginHorizontal:moderateScale(2), alignSelf:'center', backgroundColor:'transparent', width:responsiveWidth(48),height:responsiveHeight(25)}}
            >

             <View style={{flex:1}}>
                <FastImage 
                resizeMode='center'
                source={{uri:data.img}}
                style={{ borderRadius:moderateScale(1), marginVertical:moderateScale(3), alignSelf:'center', width:responsiveWidth(42),height:responsiveHeight(15)}}
                />
                <View  style={{ alignSelf:'center',justifyContent:'center',alignItems:'center',width:responsiveWidth(48),height:responsiveHeight(4),backgroundColor:barColor}} >
                <Text style={{color:'white',fontSize:responsiveFontSize(7),fontStyle:'italic'}} >{isRTL?" "+data.arabicname:data.categoryname}</Text>
                </View>
             </View>
           
            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
});

const mapDispatchToProps = {
    AddProductToBacket,
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryCard);
