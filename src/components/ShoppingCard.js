import React,{Component} from 'react';
import {View,TouchableOpacity,Text,Alert} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {AddProductToBacket} from '../actions/OrderAction';

import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class ShoppingCard extends Component {
     state={
        puy:false
     } 
//
    render(){
        const {installment,data,months,isRTL,navigator,removeProduct,count} = this.props;
        console.log('data')
        console.log(data)
        return(
            <MyButton 
            onPress={()=>{
                navigator.push({
                   screen: 'ProductDetails',
                   passProps:{
                       product:data,
                       update:true,
                       offer:data.hasOffer,
                   }
               })
            }}
            style={{ marginVertical:moderateScale(5),marginHorizontal:moderateScale(2), alignSelf:'center', backgroundColor:'transparent', width:responsiveWidth(48),height:responsiveHeight(47)}}
            >

             <View style={{width:responsiveWidth(48),height:responsiveHeight(47)}}>
                <FastImage 
                source={{uri:data.img[0]}}
                style={{ borderRadius:moderateScale(1), marginVertical:moderateScale(3),marginTop:moderateScale(8), alignSelf:'center', width:responsiveWidth(40),height:responsiveHeight(18)}}
                />

                <Text style={{alignSelf:'center', color:'#0c246b',fontSize:responsiveFontSize(6.5),fontStyle:'italic' }} >{data.name}</Text>
                {/* #f0f215 */}
                <Text style={{marginTop:moderateScale(2), alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'#ff1c8e',fontSize:responsiveFontSize(6),fontStyle:'italic' }} >{count} {strings.pices}</Text>
                
                <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>
                     <View>
                     <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'#0c246b',fontSize:responsiveFontSize(6),fontStyle:'italic' }} >{strings.total} {data.hasOffer?data.offer*count:data.installmentPrice*count} {strings.pe}</Text>
                     <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'#82C141',fontSize:responsiveFontSize(6),fontStyle:'italic' }} >{months} {strings.months}</Text>
                     <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(4), color:'red',fontSize:responsiveFontSize(6),fontStyle:'italic' }} >{installment}  {strings.perMonth}</Text>
                     </View>
                   {/* <TouchableOpacity
                    onPress={()=>{
                        removeProduct()
                    }}
                    >
                        <Icon name="delete" type="AntDesign" style={{color:'red' }} />
                    </TouchableOpacity>
                */}
                </View>
                <TouchableOpacity 
                  onPress={()=>{
                    removeProduct()
                }}
                style={{alignItems:'center',justifyContent:'center', backgroundColor:'#e54051',position:'absolute',right:0,left:0,bottom:0,height:responsiveHeight(6),width:responsiveWidth(48)}}>
                    <Icon name="delete" type="AntDesign" style={{color:'white' }} />
                </TouchableOpacity>
             </View>
           
            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    AddProductToBacket,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCard);
