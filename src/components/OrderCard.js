import React,{Component} from 'react';
import {View,ImageBackground,Text,Alert,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import {Button,Icon,Thumbnail} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import moment from 'moment'
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class OrderCard extends Component {
     

    render(){
        const {data,isRTL,navigator,onLongPress} = this.props;
        
        return(
            <View style={{elevation:10,marginVertical:moderateScale(2), alignSelf:'center', backgroundColor:'white', width:responsiveWidth(97)}}>
                <View style={{alignItems:'center', margin:moderateScale(5),flexDirection:isRTL?'row-reverse':'row'}}>
                    <Thumbnail small source={require('../assets/imgs/mtqst.png')} />
                    <Text style={{marginHorizontal:moderateScale(3)}} >{strings.qsatha}</Text>
                </View>

                {data.productOrders.map((order,index)=>(
                    <TouchableOpacity

                    onLongPress={()=>{
                        if(onLongPress){
                            navigator.push({
                                screen: 'BillDetails',
                                animated:true,
                                passprops:{
                                    product:order,
                                    flag:true
                                }
                            })
                        }

                        
                    }}

                    onPress={()=>{
                        navigator.push({
                           screen: 'ProductDetails',
                           passProps:{
                               product:data.productOrders[index].product,
                               hide:true
                           }
                       })
                    }}
                     style={{backgroundColor:'#f9f5f5', alignSelf:'center', marginVertical:moderateScale(1),alignItems:'center',width:responsiveWidth(93),height:responsiveHeight(10)}}>              
                <View style={{flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(93),alignItems:'center',flex:1}}>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{color:colors.darkPrimaryColor, fontStyle:'italic',fontSize:responsiveFontSize(6)}} >
                       {strings.name}
                    </Text>
                    </View>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{color:colors.darkPrimaryColor, fontStyle:'italic',fontSize:responsiveFontSize(6)}}>
                        {strings.pices}
                    </Text>
                    </View>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{color:colors.darkPrimaryColor, fontStyle:'italic',fontSize:responsiveFontSize(6)}}>
                        {strings.months}
                    </Text>
                    </View>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{color:colors.darkPrimaryColor, fontStyle:'italic',fontSize:responsiveFontSize(6)}}>
                        {strings.perMonth}
                    </Text>
                    </View>
                </View>

                <View style={{flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(93),alignItems:'center',marginHorizontal:moderateScale(1),flex:1}}>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{fontStyle:'italic'}}>
                      {order.product.name}
                    </Text>
                    </View>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{fontStyle:'italic'}}>
                    {order.count}
                    </Text>
                    </View>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{fontStyle:'italic'}}>
                    {order.monthCount}
                    </Text>
                    </View>
                    <View style={{ flex:1,alignItems:'center'}} >
                    <Text style={{fontStyle:'italic'}}>
                    {order.costPerMonth}
                    </Text>
                    </View>
                </View>    
                </TouchableOpacity>

                ))}

                
                

                <View style={{margin:moderateScale(7)}} >
                    <Text style={{color:colors.darkPrimaryColor}} >{strings.totalPrice} {data.total}</Text>
                </View>

                <View style={{margin:moderateScale(7),alignSelf:isRTL?'flex-start':'flex-end'}} >
                    <Text>{moment(data.createdAt).fromNow()}</Text>
                </View>
            
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

export default connect(mapStateToProps)(OrderCard);
