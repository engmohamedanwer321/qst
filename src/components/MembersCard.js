import React,{Component} from 'react';
import {View,StyleSheet,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import FastImage from 'react-native-fast-image'
import {Card,Thumbnail,Badge,Button} from 'native-base';
import moment from 'moment'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Swiper from 'react-native-swiper';
import strings from '../assets/strings';
import * as colors from '../assets/colors';

class MembersCard extends Component {
    render(){
        const {isRTL,data,navigator} = this.props;
        return(
           <TouchableOpacity
            onPress={()=>{
                navigator.push({
                    screen:'OthersProfile',
                    passProps:{user:data}
                })
            }}
           >
           <Card style={{alignSelf:'center',borderRadius:moderateScale(5), width:responsiveWidth(95),height:responsiveHeight(30)}}>
               <View style={{width:responsiveWidth(92),alignItems:'center', alignSelf:isRTL? 'flex-end':'flex-start', marginVertical:moderateScale(5),marginHorizontal:moderateScale(3),flexDirection:isRTL?'row-reverse':'row'}}>
                    <Thumbnail large source={{uri:data.img}} />
                    
                    <View style={{width:responsiveWidth(60),height:responsiveHeight(15),justifyContent:'space-between', flexDirection:isRTL?'row-reverse':'row'}}>
                    <View style={{justifyContent:'center',alignItems:'center', marginHorizontal:moderateScale(5)}}>
                        <Text style={{marginBottom:moderateScale(5),fontSize:responsiveFontSize(2.6)}}>{data.firstname} {data.lastname}</Text>
                    </View>
                    </View>
               </View>

               <View style={{marginTop:moderateScale(0), alignItems:'center'}}>
                    <View style={{flexDirection:isRTL?'row-reverse':'row'}}>
                        <Text style={{width:responsiveWidth(20),textAlign:'center', fontSize:responsiveFontSize(2.5), color:colors.buttonColor}}>{strings.job}</Text>
                        <Text  style={{width:responsiveWidth(25),textAlign:'center', fontSize:responsiveFontSize(2.5),color:colors.buttonColor,marginHorizontal:moderateScale(15)}}>{strings.phone}</Text>
                        <Text style={{width:responsiveWidth(20),textAlign:'center',fontSize:responsiveFontSize(2.5),color:colors.buttonColor}}>{strings.firm}</Text>
                    </View>
                    <View style={{marginTop:moderateScale(1), flexDirection:isRTL?'row-reverse':'row'}}>
                        <Text style={{width:responsiveWidth(20),textAlign:'center'}}>{data.type}</Text>
                        <Text  style={{width:responsiveWidth(25),textAlign:'center',marginHorizontal:moderateScale(15)}}>{data.phone}</Text>
                        <Text style={{width:responsiveWidth(20),textAlign:'center'}}>FCI</Text>
                    </View>
               </View>

           </Card>
           </TouchableOpacity> 
        );
    }
}

const styles = StyleSheet.create({
    card: {
        justifyContent:'center',
        alignItems:'center',
        height:responsiveHeight(16),
        width: responsiveWidth(25),
        borderRadius: moderateScale(1.5),
        backgroundColor:'white',
        elevation:2,
        shadowOffset:{width:1,height:2},
        marginHorizontal: moderateScale(5),
    },
    img: {
        width: responsiveWidth(18),
        height: responsiveHeight(11)
    }
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(MembersCard);
