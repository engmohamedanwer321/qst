import {
    GET_OWNER_PRODUCTS,GET_OWNER_PRODUCTS_FAILD,CURRENT_PRODUCT,
    GET_TOP_PRODUCT,GET_TOP_PRODUCT_SUCCESS,GET_TOP_PRODUCT_FAILD,LOGOUT,
    FETCH_FAV_PRODUCT_REFRESH,FETCH_FAV_PRODUCT_REQUEST,FETCH_FAV_PRODUCT_SUCCESS,FETCH_FAV_PRODUCT_FAIL
} from '../actions/types';
import {
    DataProvider,
} from 'recyclerlistview';

const initState = {
    errorText:null,
    loading:false,
    favProducts: new DataProvider(),
    pages:null,
    refresh: false,
    currentProduct: {}
}

const ProductReducer = (state=initState, action) => {
    switch(action.type){
        case GET_TOP_PRODUCT:
            return {...state,productLoading:true};  
        case GET_TOP_PRODUCT_SUCCESS:
            return {...state,topProduct:action.payload,productLoading:false};  
        case GET_TOP_PRODUCT_FAILD:
            return {...state,productLoading:false,errorText:action.payload};


        case FETCH_FAV_PRODUCT_REQUEST:
            return { ...state,errorText:null, loading: true, refresh: false };
        case FETCH_FAV_PRODUCT_SUCCESS:
            console.log()
            return {
                ...state,
                favProducts: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(state.refresh ? [...action.payload] : [...state.favProducts.getAllData(), ...action.payload]),
                loading: false,
                refresh: false,
                pages: action.pages,
                errorText:null,
            };
        case FETCH_FAV_PRODUCT_FAIL:
            return { ...state, loading: false, refresh: false, errorText: action.payload };
        case FETCH_FAV_PRODUCT_REFRESH:
            return { ...state,errorText:null, refresh: true, loading: false };
        
        case CURRENT_PRODUCT: 
            return {...state,currentProduct:action.payload}




        case GET_OWNER_PRODUCTS:
            return{ ownerProducts: action.payload}
        case GET_OWNER_PRODUCTS_FAILD:
            return{ errorText: action.payload}        

        case LOGOUT:
            return initState;                       
        default: 
            return state; 
    }
}

export default ProductReducer;