import {CATEGORY_LOAD,CATEGORY_SUCCESS,CATEGORY_FAILD,LOGOUT} from '../actions/types';

const initState = {
   categoryLoad:false,
   categories:[],
   errorText:null,
}

const CategoryReducer = (state=initState, action) => {
    switch(action.type){
        case CATEGORY_LOAD:
            return {...state,categoryLoad:true};  
        case CATEGORY_SUCCESS:
            return {...state,categories:action.payload,categoryLoad:false};  
        case CATEGORY_FAILD:
            return {...state,categoryLoad:false,errorText:action.payload}; 
        case LOGOUT:
            return initState;                     
        default: 
            return state; 
    }
}

export default CategoryReducer;