import { 
    CLIENT_TYPE,SIGNUP_REQUEST,GO_ROOT,
    SIGNUP_SUCCESS,SIGNUP_FAIL,GO_BACK,GO_NEXT,LOGOUT
 } from '../actions/types';

const initState = {
    user: [],
    errorText:null,
    clientType: null,
    signupLoading: false,
    step:1,
}

const SignupReducer = (state=initState, action) => {
    switch(action.type){
        case CLIENT_TYPE:
            return {...state,clientType:action.payload};  
        case SIGNUP_REQUEST:
            return {...state,signupLoading:true};  
        case SIGNUP_SUCCESS:
            return {...state,signupLoading:false,};  
        case SIGNUP_FAIL:
            return {...state,signupLoading:false,errorText:action.payload} 
        case GO_BACK:
            return {...state,step:state.step-1};
        case GO_NEXT:
            return {...state,step:state.step+1}; 
        case GO_ROOT: 
            return {...state,step:1};    
        case LOGOUT:
            return initState;                       
        default: 
            return state; 
    }
}

export default SignupReducer;