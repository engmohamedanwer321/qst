import * as types from "../actions/types"
import * as colors from '../assets/colors' 

const initialState = {
    unSeen:0,
    open:false,
    image:''
}

const ChatReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CHAT:
            console.log('in Chat reducer =>'+action.payload)
            return { ...state, unSeen: action.payload };    
        case types.OPEN_IMAGE:
            console.log('open Chat reducer =>'+action.payload)
            return { ...state, open:action.payload, image:action.img };        
        default:
            return state;
    }

}

export default ChatReducer;