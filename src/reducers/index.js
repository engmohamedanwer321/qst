import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form';
import MenuReducer from "./MenuReducer";
import NavigationReducer from './NavigationReducer';
import AuthReducer from "./AuthReducer";
import LanguageReducer from './LanguageReducer';
import SignupReducer from './SignupReducer';
import ProductReducer from './ProductReducer';
import NotificationsReducer from './NotificationsReducer';
import OrderReducer from './OrderReducer';
import CategoryReducer from './CategoryReducer';
import ChatReducer from './ChatReducer'


export default combineReducers({
    form: formReducer,
    menu: MenuReducer,
    navigation: NavigationReducer,
    auth: AuthReducer,
    lang:LanguageReducer,
    signup: SignupReducer,
    product: ProductReducer,
    noti: NotificationsReducer,
    order: OrderReducer,
    category: CategoryReducer,
    chat: ChatReducer,
});