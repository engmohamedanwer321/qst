

import * as types from "../actions/types";



const initialState = {

    item: ["HOME"],
    logoutLoading:false,
}

const MenuReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.SELECT_MENU:
            console.log('screens')
            console.log(action.payload)
            console.log(state.item)
            return { item: [...state.item,action.payload] };
        case types.POP_ITEM:
                state.item.pop()
                return { item: [...state.item] };
        case types.LOGOUT_LOADING:
            return {...state, logoutLoading:true };
        case types.LOGOUT_LOADING_END:
            return {...state, logoutLoading:false };    
        case types.LOGOUT:
            return initialState; 
        default:    
            return state;
    }

}



export default MenuReducer;