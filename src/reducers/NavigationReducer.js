import * as types from "../actions/types"

const initialState = {
  screen_name: '',
};

const NavigationReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_NAVIGATION_SCREEN_NAME:
      return { ...state, screen_name: action.payload };
    case types.LOGOUT:
      return initialState;

    default:
      return state;
  }
};

export default NavigationReducer;
