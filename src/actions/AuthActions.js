import { AsyncStorage } from 'react-native';
import axios from 'axios';
import {
   LOGIN_SUCCESS, LOGIN_FAIL, LOGIN_REQUEST, CHECK_USER_TYPE,CURRENT_USER,USER_TOKEN,
   SIGNUP_REQUEST,SIGNUP_SUCCESS,SIGNUP_FAIL,USER_LOCATION
   } from './types';
import { LOGIN, BASE_END_POINT } from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import Strigs from '../assets/strings';

//
export function login(email, password, FB_token, navigator) {
  return (dispatch,getState) => {    
    dispatch({ type: LOGIN_REQUEST });
    axios.post(`${BASE_END_POINT}signin`, JSON.stringify({
        email: email,
        password: password,
      }), {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      axios.post(`${BASE_END_POINT}addToken`, JSON.stringify({
        token: FB_token 
      }), {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${res.data.token}`
      },
    }).then(response => {
      console.log("5ي5ي5ي55ي5يي5");
      console.log(res.data);   
      AsyncStorage.setItem('@QsathaUser', JSON.stringify(res.data));  
      dispatch({ type: LOGIN_SUCCESS, payload: res.data});  
        navigator.resetTo({
          screen: 'Home',
          animated: true,
        });  
      
    }).catch(error => {
      console.log('inner');
        console.log(error);
      if (!error.response) {
        dispatch({
          type: LOGIN_FAIL,
          payload: Strigs.noConnection,
        });
      } 
    });
    
    })
      .catch(error => {
        if(getState().auth.user){

          return
        }
        console.log('outer');
        console.log(error.response);
        if (!error.response) {
          dispatch({
            type: LOGIN_FAIL,
            payload: Strigs.noConnection,
          });
        } else if (error.response.status == 401) {
          dispatch({
            type: LOGIN_FAIL,
            payload: Strigs.noConnection,
          });
         RNToasty.Error({title: Strigs.loginError})
        }
      });
  };
}

export function getUser(user){
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}

export  function signup(user,navigator) {
  return (dispatch) => {       
    dispatch({ type: SIGNUP_REQUEST });
    console.log('my data   ');
    console.log(user);

    axios.post(`${BASE_END_POINT}signup`, JSON.stringify(user), {
      headers: {
        'Content-Type': 'application/json',
        //'Content-Type': 'multipart/form-data',
      },
    }).then(response => {
      RNToasty.Success({title:Strigs.addUserSuccessfuly})
      //AsyncStorage.setItem('@QsathaUser', JSON.stringify(res.data));  
      console.log(response.data);
      console.log('done'); 
      dispatch({ type: SIGNUP_SUCCESS});  
      navigator.resetTo({
        screen:'Login'
      })            
    })
      .catch(error => {
          dispatch({type: SIGNUP_FAIL});
        console.log(error.response);
        if (!error.response) {
          dispatch({payload:Strigs.noConnection})
        }else if (error.response.status == 422) {
          dispatch({type: SIGNUP_FAIL, payload: Strigs.signupFail})
        }else{
          dispatch({type: SIGNUP_FAIL, payload: Strigs.loginError})
        }
      });
  };
}

export function userToken(token){
  return dispatch => {
    dispatch({type:USER_TOKEN,payload:token})
  }
}

export function userLocation(postion) {
  return dispatch => {
    console.log(postion)
    dispatch({type:USER_LOCATION,payload:postion})
  }
  
}

