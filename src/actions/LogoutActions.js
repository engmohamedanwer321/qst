import { LOGOUT,LOGOUT_LOADING,LOGOUT_LOADING_END } from "./types";
import { BASE_END_POINT } from '../AppConfig';
import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';


export default function logout(FB_token, BE_token, navigator) {
    console.log('dddddddddddddqqqq')
    return  dispatch => {
        dispatch({type:LOGOUT_LOADING})
        axios.post(`${BASE_END_POINT}logout`, JSON.stringify({
            token: FB_token 
          }), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${BE_token}`
          },
        }).then(response => {
            console.log('log out')
            console.log(response);
            
             AsyncStorage.removeItem("@QsathaUser");
            if (Platform.OS === 'android') {
                navigator.resetTo({
                    screen: 'Login',
                    animated: true
                })       
            }
            else {
                navigator.resetTo({
                    screen: 'Login',
                    animated: true
                })
            }

            dispatch({ type: LOGOUT })
          
        }).catch(error => {
            console.log('log out error')
            console.log(error)
            console.log(error.response)
            dispatch({type:LOGOUT_LOADING_END})
        });
        
    }
}