
import { SELECT_MENU,POP_ITEM } from "./types";



export  function selectMenu(item) {
    return dispatch => {
        dispatch({type: SELECT_MENU, payload: item})
    }
}

export  function removeItem(item) {
    return dispatch => {
        dispatch({type: POP_ITEM})
    }
}