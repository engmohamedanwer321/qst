import {
    USER_LOCATION,ADD_PRODUCT_TO_ORDER,REMOVE_PRODUCT_FROM_ORDER,
    CLEAR_BACKET,PUT_LOCAL_ORDER
} from '../actions/types';

export function userLocation(location){
    return dispatch => {
        console.log('ans')
        dispatch({type:USER_LOCATION,payload:location});
    }
}
//
export function AddProductToBacket(product,productInfo){
    return dispatch => {
        console.log('ans')
        dispatch({type:ADD_PRODUCT_TO_ORDER,payload:product,productInfo:productInfo});
    }
}

export function RemoveProductFromBacket(productID){
    return dispatch => {
        console.log('ans Remove')
        dispatch({type:REMOVE_PRODUCT_FROM_ORDER,payload:productID});
    }
}

export function ClearBacket(){
    return dispatch => {
        console.log('CLEAR_BACKET')
        dispatch({type:CLEAR_BACKET});
    }
}

export function putLocalOrder(order,orderData){
    return dispatch => {
        console.log("EXSIT ORDER")
                console.log(order)
                console.log(orderData)
        dispatch({type:PUT_LOCAL_ORDER,payload:order,orderData:orderData});
    }
}