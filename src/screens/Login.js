import React, { Component } from 'react';
import {
  View,TouchableOpacity,Keyboard,Image
} from 'react-native';
import { connect } from 'react-redux';
import {Button} from 'native-base';
import {login} from '../actions/AuthActions';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { Field, reduxForm } from "redux-form"
import SnackBar from 'react-native-snackbar-component';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {removeItem} from '../actions/MenuActions';


const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);


const validate = values => {
    const errors = {};

    const password = values.password;

    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }
    
    if (password == null) {
        errors.password = Strings.require;
    }

    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class Login extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    };
    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator;
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
    }
    
    componentDidMount(){    
        this.disableDrawer(); 
        console.log('anwer  '+this.props.isRTL) 
        console.log('Login page user token  '+this.props.userToken)     
    }

    componentWillUnmount(){
        this.props.removeItem()
      }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }


    onLogin(values) {
        //AsyncStorage.setItem('@password', values.password);
        this.props.login(
            values.email.toLowerCase(),
            values.password,
            this.props.userToken,
            this.props.navigator,
        );
       
    }


    renderLoginButtons() {
        const { handleSubmit,barColor } = this.props;

        return (
            <MyButton
            onPress={handleSubmit(this.onLogin.bind(this))}
            style={{marginTop:hp(5), alignSelf:'center', justifyContent:'center',alignItems:'center',height:hp(8),width:wp(50),backgroundColor:barColor,borderRadius:wp(10)}}
            >
                <AppText fontSize={wp(5)} text={Strings.login} color='white' />
            </MyButton>
        );
    }

    renderForgetPassword() {
        return (
            <MyTouchableOpacity
             onPress={()=>{
                this.props.navigator.push({
                    screen: 'ForgetPassword',
                    animated: true,
                });
             }}
            style={{marginTop:hp(3), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80)}}
            >
                <AppText fontSize={responsiveFontSize(7)} text={Strings.forgetPassword} color='black' />
            </MyTouchableOpacity>
        );
    }

   
    renderContent() {
        return (
            <View>
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="email" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent}
                 returnKeyType="done"
                    onSubmit={() => {
                       Keyboard.dismiss()
                    }}
                />

                <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="password" isRTL={this.props.isRTL} type="password" password={true} label={Strings.password} component={InputComponent}
                    returnKeyType="done"
                    inputRef={el => this.passwordField = el }
                    onSubmit={() => {
                        Keyboard.dismiss()
                    }}
                />

                {this.renderLoginButtons()}
                {/*this.renderForgetPassword()*/}
            </View>

        )
    }

    render(){
        const {barColor} = this.props
        return(
            <View style={{ flex:1 }} >
               <Image style={{alignSelf:'center',marginTop:hp(10), width:wp(80),height:hp(25)}} source={require('../assets/imgs/mtqst.png')} />
               <View style={{marginTop:hp(4),width:wp(80),alignSelf:'center'}}>
                        {this.renderContent()}
                </View>
                
              <View style={{justifyContent:'center',alignItems:'center', flexDirection:this.props.isRTL?'row-reverse':'row', alignSelf:'center',marginTop:moderateScale(15)}}>
                    <AppText fontSize={responsiveFontSize(7)} text={Strings.dontHaveAccount} color='gray' />
                    <MyTouchableOpacity
                    onPress={()=>{
                        this.props.navigator.push({
                            screen: 'Signup',
                            animated: true,
                        });
                    }}
                    >
                        <AppText fontSize={responsiveFontSize(7)} text={Strings.registerNow} color={colors.darkPrimaryColor} />
                    </MyTouchableOpacity>
                </View>
             
            
                {this.props.loading&&<LoadingDialogOverlay title={Strings.checkLogin} />}
               
            </View>
        );
    }
}

const form = reduxForm({
    form: "Login",
    validate,
})(Login)

const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    userToken: state.auth.userToken,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

