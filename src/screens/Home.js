import React,{Component} from 'react';
import {
     View,TouchableOpacity,NetInfo,ScrollView,FlatList,Text,Platform,AppState,
     Alert,AsyncStorage,ImageBackground
} from 'react-native';
import {  moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import {Badge,Icon } from 'native-base';
import AppHeader from '../common/AppHeader'
//import Icon from 'react-native-vector-icons/FontAwesome5';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';

import LottieView from 'lottie-react-native';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'; 
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ProductCard from '../components/ProductCard';
import AdsCard from '../components/AdsCard';
import MobileCard from '../components/MobileCard';
import {userLocation} from '../actions/OrderAction';
import Permissions from 'react-native-permissions';
import {selectMenu,removeItem} from '../actions/MenuActions';
import {getUnseenMessages} from '../actions/ChatAction'

/*import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
  } from 'react-native-admob'
 */ 
  //ca-app-pub-9566226877101037~2778204178

  //ca-app-pub-9566226877101037/2119852334
  let that;
class Home extends Component {
     that = this;
    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor:that.props.color,
    };

    list=[];
    page=1;
    page2=1;
    state = { 
        topProducts:[],
        topProductsLoading: true,
        topAds:[],
        topAdsLoading: true,
        category: [],
        clothes:[],
        clothesLoading:true,
        mobiles:[],
        mobilesLoading:true, 
        noConnection:null,
    };

    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });    
        console.log('Color   ',this.props.color)
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
              this.getTopProduct()
              this.getTopAds()
              this.getCategory()
              this.props.getUnseenMessages(this.props.currentUser.token)
              this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
            }else{
                this.setState({noConnection:'Strings.noConnection'})
            }
          });
      }

      componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }

      permissions = () => {
        Permissions.check('location').then(response => {
            if (response === 'denied' || response === 'undetermined') {
                this._requestPermission()
            } else {
                console.log('permission done')
                this.getLocation();
            }
        });
    }
    
    _requestPermission = () => {
        Permissions.request('location').then(response => {
            if (response === 'denied' || response === 'undetermined' ) {
                this._requestPermission()
            } else {
                console.log('permission done')
                this.getLocation();
            }
        })
    }
    
    getLocation = () => {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            console.log('region is 222  =>  ')
            console.log(position)
            this.props.userLocation([position.coords.latitude,position.coords.longitude])
          }, (error)=>console.log(error));
    }
    
    checkPermissionForApiLess23 = async () => {
        try{
            const p = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
            if(p != PermissionsAndroid.RESULTS.GRANTED){
                this.putPermissionForApiLess23()
            }else{
               console.log('pppppppppprrrrr')
               this.getLocation()             
            }
        }catch (err) {
            console.log(err)
          }
    }
    putPermissionForApiLess23 = async () => {
        try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.getLocation()
            } else {
                this.putPermissionForApiLess23()
            }
    
            
          } catch (err) {
            console.log(err)
          }
    }


    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    
    
    componentDidMount(){
        //Alert.alert("hi")
      
        this.enableDrawer();
        if(Platform.Version < 23){
            this.checkPermissionForApiLess23()
        }else{
            this.permissions();
        }
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})                   
                     this.getTopProduct()
                     this.getTopAds()
                     this.getCategory()
                     this.props.getUnseenMessages(this.props.currentUser.token)
                     this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
                     
                }
            }
          );
 
        
    }

    componentWillUnmount(){
      this.props.removeItem()
    }

    
    leftComponent = () => (
        <View style={{flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <TouchableOpacity
            onPress={()=>{
                if(this.props.currentUser){
                    this.props.navigator.push({
                        screen:'Notifications',
                        animated:true,
                      })
                    this.props.selectMenu('NOTIFICATION');
                }else{
                    RNToasty.Warn({title:strings.checkUser}) 
                }
            }}

             style={{marginHorizontal:moderateScale(7)}}>
                <Badge danger style={{backgroundColor:this.props.barColor=='#D35400'? '#353B48':'red', elevation:2, height:20, justifyContent:'center',alignItems:'center'}}>
                        <Text style={{alignSelf:'center', fontSize:10, color:'white'}}>{this.props.unReaded}</Text> 
                    </Badge>
                <Icon style={{color:'white',fontSize:20, marginTop:moderateScale(-3)}} type='FontAwesome5' name='bell' />
            </TouchableOpacity>

            <TouchableOpacity
            onPress={()=>{
                if(this.props.currentUser){
                    this.props.navigator.push({
                        screen:'ChatPeople',
                        animated:true,
                      })
                    this.props.selectMenu('CHAT');
                }else{
                    RNToasty.Warn({title:strings.checkUser}) 
                }
            }}
             style={{marginHorizontal:moderateScale(7)}}>
                <Badge danger style={{backgroundColor:this.props.barColor=='#D35400'? '#353B48':'red', elevation:2, height:20, justifyContent:'center',alignItems:'center'}}>
                    <Text style={{alignSelf:'center', fontSize:10, color:'white'}}>{this.props.unSeen}</Text> 
                </Badge>
                <Icon style={{color:'white',fontSize:20, marginTop:moderateScale(-3)}} type='Entypo' name='chat' />
            </TouchableOpacity>

           
        </View>
    )

   

   
    /************** from ?Here  ****************/
    

    getTopProduct = () =>{
        this.setState({topProductsLoading:true})
        axios.get(`${BASE_END_POINT}products/?top=true`)
        .then(response=>{
            console.log('Top Products')
            console.log(response.data.data)
            this.setState({topProducts:response.data.data,topProductsLoading:false})
        }).catch(error=>{
            this.setState({topProductsLoading:false})
            console.log('Top Products error')
            console.log(error.response)
        })
    }

    getClothes = (id) =>{
        this.setState({clothesLoading:true})
        axios.get(`${BASE_END_POINT}products/?categoryId=${id}&salesRatio&visible=true&page=1&limit=5`)
        .then(response=>{
            console.log('clothes')
            console.log(response.data.data)
            this.setState({clothes:response.data.data,clothesLoading:false})
        }).catch(error=>{
            this.setState({clothesLoading:false})
            console.log('clothes error')
            console.log(error.response)
        })
    }

    getMobiles = (id) =>{
        this.setState({mobilesLoading:true})
        axios.get(`${BASE_END_POINT}products/?categoryId=${id}&salesRatio&visible=true&page=1&limit=5`)
        .then(response=>{
            console.log('mobiles')
            console.log(response.data.data)
            this.setState({mobiles:response.data.data,mobilesLoading:false})
        }).catch(error=>{
            this.setState({mobilesLoading:false})
            console.log('mobiles error')
            console.log(error.response)
        })
    }

    getCategory = () =>{
        this.setState({topProductsLoading:true})
        axios.get(`${BASE_END_POINT}categories`)
        .then(response=>{
            console.log('categoty')
            console.log(response.data.data)
            const homeCat=[]
             response.data.data.map(cat=>{
                if(cat.categoryname.toLowerCase().includes('laptop')||cat.categoryname.toLowerCase().includes('لابتوب')){
                    homeCat[0]= 
                    this.getClothes(cat.id)
                }
                if(cat.categoryname.toLowerCase().includes('mobiles')||cat.categoryname.toLowerCase().includes('موبايلات')){
                    this.getMobiles(cat.id)
                }
            })
        }).catch(error=>{
            console.log('categoty error')
            console.log(error.response)
        })
    }

    getTopAds = () =>{
        this.setState({topAdsLoading:true})
        axios.get(`${BASE_END_POINT}ads?top=true`)
        .then(response=>{
            console.log('Top ADs')
            console.log(response.data.data)
            this.setState({topAds:response.data.data,topAdsLoading:false})
        }).catch(error=>{
            this.setState({topAdsLoading:false})
            console.log('Top ADs error')
            console.log(error.response)
        })
    }

    renderTopProduct = () => {
        const {isRTL,navigator} = this.props;
        return(
            <View>
               {
                   this.state.topProductsLoading?
                   <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:wp(100),height:hp(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>
                   :
                <View>
                    <View style={{alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start'}}>
                        <Text style={{color:colors.darkPrimaryColor,fontSize:responsiveFontSize(6),fontStyle:'italic',fontWeight:'600' }} >{strings.topProduct}</Text>
                        <Icon name='checkcircleo' type='AntDesign' style={{marginHorizontal:moderateScale(3), color:'#6b8fdb',fontSize:responsiveFontSize(7)}}  />
                        </View>
                    <FlatList 
                    data={this.state.topProducts}
                    renderItem={({item})=><ProductCard navigator={navigator}  data={item}/>}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    inverted={isRTL?-1:0}
                   />
                </View>
               }
            </View>
        )
    }
    renderTopAds = () => {
        const {navigator} = this.props;
        return(
            <View>
               
               {this.state.topAdsLoading?
               <View style={{ flex:1}}> 
               <LottieView
               style={{width:wp(100),height:hp(33.3)}}
                   source={require('../assets/animations/smartGarbageLoading.json')}
                   autoPlay
                   loop
                   />
               </View>
               :
               <View>
                <AdsCard
                navigator={navigator}
                 ads={this.state.topAds} />
               </View>
               }
            </View>
        )
    }
    renderClothes = () => {
        const {isRTL,navigator} = this.props;
        return(
            <View>
               
                {
                    this.state.clothesLoading?
                    <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:wp(100),height:hp(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>
                    :
                    <View>
                         <View style={{alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start'}}>
                            <Text style={{color:colors.darkPrimaryColor,fontSize:responsiveFontSize(6),fontStyle:'italic',fontWeight:'600' }} >{strings.laptop}</Text>
                            <Icon name='mobile1' type='AntDesign' style={{marginHorizontal:moderateScale(3), color:'#6b8fdb',fontSize:responsiveFontSize(7)}}  />
                        </View>
                        <FlatList 
                        // numColumns={2}
                        data={this.state.clothes}
                        renderItem={({item})=><ProductCard navigator={navigator} data={item} />}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        inverted={isRTL?-1:0}
                        />
                    </View>

                }
               
            </View>
        )
    }
    renderMobiles = () => {
        const {isRTL,navigator} = this.props;
        
        return(
            <View>
               {this.state.mobilesLoading?
                <View style={{ flex:1}}> 
                <LottieView
                style={{width:wp(100),height:hp(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
               :
               <View>
               <View style={{alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start'}}>
                  <Text style={{color:colors.darkPrimaryColor,fontSize:responsiveFontSize(6),fontStyle:'italic',fontWeight:'600' }} >{strings.mobiles}</Text>
                  <Icon name='mobile1' type='AntDesign' style={{marginHorizontal:moderateScale(3), color:'#6b8fdb',fontSize:responsiveFontSize(7)}}  />
              </View>
              <FlatList 
              // numColumns={2}
              data={this.state.mobiles}
              renderItem={({item})=><MobileCard  data={item} navigator={navigator}/>}
              horizontal
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              inverted={isRTL?-1:0}
              />
              </View>
               }
            </View>
        )
    }


    render(){
        const {currentUser,unSeen} = this.props;
        return(
            <View showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ flex:1,backgroundColor:'white'}}>
                
                <AppHeader  title={strings.home} showCart  showBurger leftComponent={currentUser&&this.leftComponent()} navigator={this.props.navigator} />                   
              {/* <AdMobBanner
                adSize="fullBanner"
                adUnitID="ca-app-pub-9566226877101037/9926934362"
                testDevices={[AdMobBanner.simulatorId]}
                onAdFailedToLoad={error => console.error(error)}
                />
              */}
               
                {
                    this.state.noConnection?
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                        <Text style={{fontSize:20,color:'black'}} >{strings.noConnection}</Text>
                        <Text style={{fontSize:20,color:'black'}} >:(</Text>
                    </View>
                    :
                    <ScrollView  style={{backgroundColor:'white'}} >
                        {this.renderTopAds()}
                    {this.renderTopProduct()}
                    
                    {this.renderClothes()}
                    
                    {this.renderMobiles()}
                    </ScrollView>
                }

              
            </View>
        )
    }
}


const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,  
    unReaded: state.noti.unReaded, 
    barColor: state.lang.color,
    unSeen: state.chat.unSeen,

})

const mapDispatchToProps = {
    selectMenu,
    userLocation,
    getUnreadNotificationsNumers,
    removeItem,
    getUnseenMessages
    
}

export default connect(mapToStateProps,mapDispatchToProps)(Home);