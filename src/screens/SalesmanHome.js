import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,NetInfo,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button,Picker} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import {getCompanyProducts} from '../actions/ProductAction';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import AppSpinner from '../common/AppSpinner';
import SnackBar from 'react-native-snackbar-component';
import ClientCard from '../components/ClientCard';
import { RNToasty } from 'react-native-toasty';
import {AddProductToBacket} from '../actions/OrderAction';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import ProductCard from '../components/ProductCard'
import {removeItem} from '../actions/MenuActions';

const types=[false, 'up', 'down'];

class SalesmanHome extends Component {

    page=1;
    state= {
        index:0,
        errorText:null,
        clients: new DataProvider(),
        refresh:false,
        loading:false,
        pages:null,
        sortType: Strings.random
    }

    static navigatorStyle = {
        navBarHidden: true,
       // statusBarColor: colors.darkPrimaryColor,
    };

    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }

    constructor(props) {
        super(props);  
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });   
        this.renderLayoutProvider = new LayoutProvider(
          () => 2,
          (type, dim) => {
            dim.width = responsiveWidth(50);
            dim.height = responsiveHeight(28);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
              //  this.getClients(this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    //this.getClients(this.page,true);
                }
            }
          );      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

    getClients(page, refresh) {
      
            //this.setState({loading:true})
            //${this.props.currentUser.user.id}

            let uri = `${BASE_END_POINT}premiums/AllClients?salesMan=${this.props.currentUser.user.id}&status=PENDING&page=${page}&limit=20`;
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false,loading:true})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        clients: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.clients.getAllData(), ...response.data.data]),
                    })
                    if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error.response);
                    console.log(error);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    renderRow = (type, data, row) => {
        console.log(data)
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <ClientCard 
       data={data}
        onPress={()=>{
            this.props.navigator.push({
                screen:'ClientOrderDetails',
                animated:true,
                passProps: {
                    data:data,
                    //salesmanID:this.currentUser.user.id,
                    id:data.id
                }
            })
        }}
       data={data}
       navigator={this.props.navigator}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,currentUser,categoryName,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.myClients} showBurger navigator={navigator}/>
                {this.state.loading?
                <View style={{ flex:1}}> 
                <LottieView
                style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                source={require('../assets/animations/smartGarbageLoading.json')}
                autoPlay
                loop
                />
                 <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                <View style={{flex:1}} >
                    <RecyclerListView            
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={ new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(currentUser.user.clients) }
                    rowRenderer={this.renderRow}
                    renderFooter={this.renderFooter}
                    onEndReached={() => {
                    
                        if(this.page <= this.props.pages){
                            this.page++;
                            this.getProducts(this.page, false);
                        }
                        
                    }}
                    refreshControl={<RefreshControl colors={["#B7ED03"]}
                        refreshing={this.state.refresh}
                        onRefresh={() => {
                        this.page = 1
                        this.getProducts(this.page, true);
                        }
                        } />}
                    onEndReachedThreshold={.5}
                    />

            </View>
                }
                 <SnackBar
                visible={this.state.errorText!=null?true:false} 
                textMessage={Strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    footerSpiner: {
        alignItems: 'center',
        alignSelf: "flex-end",
        flex: 1,
        width: '100%',
        marginVertical: 10,
        backgroundColor: colors.primaryColor
      }
    
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    products: state.product.products,
    loading: state.product.loading,
    refresh: state.product.refresh,
    pages: state.product.pages,
    errorText: state.product.errorText,
    currentUser : state.auth.currentUser,
    barColor: state.lang.color 
})

const mapDispatchToProps = {
    getCompanyProducts,
    AddProductToBacket,
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(SalesmanHome);
