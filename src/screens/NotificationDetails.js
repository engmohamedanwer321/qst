import React,{Component} from 'react';
import {
     View,Image,Text,TouchableOpacity,ScrollView
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import Strings from '../assets/strings';
import AppHeader from '../common/AppHeader'






class NotificationDetails extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    };

    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });
        }
  
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    componentDidMount(){
        this.enableDrawer();
    }




    render(){
        const {data,img,title,go,barColor,navigator} = this.props;
        return(
            <View style={{flex:1}}>               
                <AppHeader title={title}   showBack navigator={this.props.navigator} />
                <ScrollView style={{flex:1}}> 
                <Image resizeMode='contain' style={{marginVertical:moderateScale(20), alignSelf:'center',marginTop:moderateScale(25), width:responsiveWidth(70),height:responsiveHeight(40)}} source={img} />
                
                <View style={{marginHorizontal:moderateScale(10),alignSelf:'center',marginTop:moderateScale(3)}}>
                   <Text>{data.arabicDescription}</Text>
                </View>

                {go&&
                <TouchableOpacity 
                 onPress={()=>{
                     navigator.push({
                         screen: 'MyOrders'
                     })
                 }}
                 style={{borderRadius:moderateScale(4), alignSelf:'center',justifyContent:'center',alignItems:'center',marginVertical:moderateScale(10),backgroundColor:barColor,width:responsiveWidth(40),height:responsiveHeight(8)}}>
                <Text style={{color:'white'}}>{Strings.seeProduct}</Text>
                </TouchableOpacity>
                }

                </ScrollView>
                

            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color 
})


export default connect(mapToStateProps)(NotificationDetails);