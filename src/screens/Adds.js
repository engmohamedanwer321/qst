import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,NetInfo} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import AdversmentCard from '../components/AdversmentCard';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import {removeItem} from '../actions/MenuActions';


class Adds extends Component {

    page=1;
    state= {
        errorText:null,
        allCategories: new DataProvider(),
        refresh:false,
        loading:true,
        pages:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
       // statusBarColor: colors.darkPrimaryColor,
    };
   
        componentDidUpdate(){
            this.props.navigator.setStyle({
                statusBarColor: this.props.barColor,
                
              }); 
          }
          componentWillUnmount(){
            this.props.removeItem()
          }

    constructor(props) {
        super(props);  
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,     
          });   
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(99);
            dim.height = responsiveHeight(29);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getAdds(this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getAdds(this.props.companyID,this.page,true);
                }
            }
          );      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    getAdds(page, refresh) {
            //this.setState({loading:true})
            let uri = `${BASE_END_POINT}ads?page=${page}&limit=20`
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false,loading:true})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        allCategories: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.allCategories.getAllData(), ...response.data.data]),
                    })
                    if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error.response);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <AdversmentCard 
       data={data}
       index={row}
        onPress={()=>{
            this.props.navigator.push({
                screen:'AddsDetails',
                animated:true,
                passProps: {
                    data:data
                }
            })
        }}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.adds} showBurger navigator={navigator}/>
                {this.state.loading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                <RecyclerListView            
                layoutProvider={this.renderLayoutProvider}
                dataProvider={this.state.allCategories}
                rowRenderer={this.renderRow}
                renderFooter={this.renderFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.props.pages){
                        this.page++;
                        this.getAdds(this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.refresh}
                    onRefresh={() => {
                      this.page = 1
                      this.getAdds(this.page, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}
        
            />
                }
                
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(Adds);
