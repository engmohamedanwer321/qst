import React,{Component} from 'react';
import {View,RefreshControl,NetInfo,Text,TextInput,TouchableOpacity,Modal} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Picker,Thumbnail} from 'native-base';
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import ProductCard from '../components/ProductCard'
import ChatPeopleCard from '../components/ChatPeopleCard';
import strings from '../assets/strings';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image';

const types=[false, 'up', 'down'];

class ChatPeople extends Component {

    page=1;
    list=[];
    state= {
        index:0,
        errorText:null,
        friendMessages: new DataProvider(),
        searchProducts: new DataProvider(),
        refresh:false,
        loading:true,
        pages:null,
        sortType: Strings.random,
        flag:0
    }

    static navigatorStyle = {
        navBarHidden: true,
       // statusBarColor: colors.darkPrimaryColor,
    };
    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
    constructor(props) {
        super(props);    
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(12);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                if(this.props.currentUser.user.type=='CLIENT')
                this.getClientFriends();
                else
                this.getSalesMAnFriends()
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    if(this.props.currentUser.user.type=='CLIENT')
                    this.getClientFriends();
                    else
                    this.getSalesMAnFriends()
                }
            }
          );      
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    getClientFriends() {
            let uri = `${BASE_END_POINT}premiums/AllClients?client=${this.props.currentUser.user.id}`
            axios.get(uri)
                .then(response => {
                    console.log("Chat USers")
                    console.log(response.data.data)
                     const f = response.data.data
                     let friends = []
                     let ids = []
                     for(var i=0;i<f.length;i++){
                        let user =  f[i]
                        if(!ids.includes(user.salesMan.id)){
                            friends.push(user)
                            ids.push(user.salesMan.id)
                        }
                     }
                     console.log("Chat USers")
                     console.log(friends)
                     console.log(ids)
                   
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        friendMessages: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(friends),
                    })
                    if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error.response);
                    console.log(error);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }


    getSalesMAnFriends() {
        let uri = `${BASE_END_POINT}premiums/AllClients?salesMan=${this.props.currentUser.user.id}`
        axios.get(uri)
            .then(response => {
                console.log("Chat USers")
                console.log(response.data.data)
                 const f = response.data.data
                 let friends = []
                 let ids = []
                 for(var i=0;i<f.length;i++){
                    let user =  f[i]
                    if(!ids.includes(user.client.id)){
                        friends.push(user)
                        ids.push(user.client.id)
                    }
                 }
                 console.log("Chat USers")
                 console.log(friends)
                 console.log(ids)
               
                this.setState({
                    loading:false,
                    refresh:false,
                    pages:response.data.pageCount,
                    errorText:null,
                    friendMessages: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(friends),
                })
                if(response.data.data.length==0){
                    RNToasty.Info({title:Strings.notResults})
                }
            }).catch(error => {
                this.setState({loading:false,refresh:false})
                console.log(error.response);
                console.log(error);
                if (!error.response) {
                    this.setState({errorText:Strings.noConnection})
                }
            })
    
}

    renderRow = (type, data, row) => {
     return (
        <ChatPeopleCard 
        onPress={()=>{
            this.props.navigator.push({
                screen:'Chat',
                animated:true,
                passProps: {data:data}
            })
        }}
       data={data}
       navigator={this.props.navigator}
        />
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,categoryName,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={strings.friends} showBurger navigator={navigator}/>
                {this.state.loading?
                <View style={{ flex:1}}> 
                <LottieView
                style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                source={require('../assets/animations/smartGarbageLoading.json')}
                autoPlay
                loop
                />
                 <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                <View style={{flex:1}} >
                   <TouchableOpacity 
                        onPress={()=>{
                            this.props.navigator.push({
                                screen:'Chat',
                                animated:true,
                                passProps: {admin:true,}
                            })
                        }}
                        style={{borderBottomColor:'#DEDEDE',borderBottomWidth:0.4, justifyContent:'center', height:responsiveHeight(12),width:responsiveWidth(100)}}>
                            <View style={{alignItems:'center', marginHorizontal:moderateScale(5),flexDirection:'row'}}>
                                <Thumbnail small source={require("../assets/imgs/mtqst.png")} />
                                <View style={{justifyContent:'center',alignItems:'center', marginHorizontal:moderateScale(3)}}>
                                <Text >QST Admin</Text>             
                                <Text style={{fontSize:10,color:'gray'}}>admin</Text>             
                                </View>
                            </View>
                    </TouchableOpacity> 

                    <RecyclerListView            
                    layoutProvider={this.renderLayoutProvider}
                    rowRenderer={this.renderRow}
                    dataProvider={this.state.friendMessages}                    
                    
                    refreshControl={<RefreshControl colors={["#B7ED03"]}
                        refreshing={this.state.refresh}
                        onRefresh={() => {
                            if(this.props.currentUser.user.type=='CLIENT')
                            this.getClientFriends();
                            else
                            this.getSalesMAnFriends()
                        }
                        } />}
                    
                    />

            </View>
                }

            </View>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps)(ChatPeople);
