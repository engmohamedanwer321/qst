import React,{Component} from 'react';
import {
     View,Text,TouchableOpacity,ScrollView
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppHeader from '../common/AppHeader'
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import { Icon } from 'native-base';


class AddsDetails extends Component {
  state = {
    images: this.props.data.img,
    index:0,
  };

  static navigatorStyle = {
    navBarHidden: true,
    //statusBarColor: colors.darkPrimaryColor
  };

  constructor(props) {
    super(props);
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,     
      });  
  }
  componentDidMount() {
    this.enableDrawer();
  }

  componentDidUpdate(){
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      }); 
  }

  enableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

  
  renderSlideShow = () => {
    const {isRTL} = this.props;
    
    return(
        <View style={{marginBottom:moderateScale(3),width:responsiveWidth(100),height:responsiveHeight(60),justifyContent:'center',alignItems:'center'}}>
            <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                <TouchableOpacity
                   onPress={()=>{
                    if(this.state.index==0){
                        this.setState({index:0})
                    }else{
                        this.setState({index:this.state.index-1})
                    }
                }} 
                >
                    <Icon name={isRTL? 'chevron-right':'chevron-left'} type='Entypo' style={{color:'#ff6f61'}} />
                </TouchableOpacity>
                <FastImage resizeMode='stretch' style={{width:responsiveWidth(55),height:responsiveHeight(40), marginHorizontal:moderateScale(15)}} source={{uri:this.state.images[this.state.index]}} />
                <TouchableOpacity
                onPress={()=>{
                    if(this.state.index==this.state.images.length-1){
                        this.setState({index:0})
                    }else{
                        this.setState({index:this.state.index+1})
                    }
                }}
                >
                    <Icon name={isRTL? 'chevron-left':'chevron-right'} type='Entypo' style={{color:'#ff6f61'}} />
                </TouchableOpacity>
            </View>

            <View style={{marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
               
                {this.state.images.map((img,i)=>(
                     <TouchableOpacity
                     onPress={()=>{
                         this.setState({index:i})
                     }}
                     style={{borderColor:this.state.index==i? '#ff6f61' : '#CCCCCC', borderWidth:1, width:responsiveWidth(18),height:responsiveHeight(8), marginHorizontal:moderateScale(2),marginVertical:moderateScale(1.5)}}>
                        <FastImage resizeMode='stretch' style={{width:responsiveWidth(18),height:responsiveHeight(8),}} source={{uri:img}} />
                    </TouchableOpacity>
                ))}
                
            </View>
        </View>
    )
}

renderProductDetails = (type,value) =>{
    const {isRTL} = this.props
    return(
        <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',flexDirection:isRTL?'row-reverse':'row' , width:responsiveWidth(100),height:responsiveHeight(7),justifyContent:'space-between',alignItems:'center'}}>
            <AppText paddingHorizontal={moderateScale(9)} text={type} color='#06134C' fontSize={responsiveFontSize(2.5)} />
            <AppText paddingHorizontal={moderateScale(9)} text={value} color={colors.skipIconColor} fontSize={responsiveFontSize(2)} />
        </View>
    )
}
  

  
  
  render() {
    const { data} = this.props;
    return (
      <ScrollView style={{ flex: 1 }}>
        <AppHeader  title={Strings.addsDetails} showBack navigator={this.props.navigator} />                   
        <FastImage 
        resizeMode='stretch'
        source={{uri:this.state.images[0]}}
        style={{width:responsiveWidth(100),height:responsiveHeight(50)}}
         />
        <View style={{alignSelf:'center', marginVertical:moderateScale(8)}} >
        <Text  style={{marginHorizontal:moderateScale(15), color:colors.skipIconColor,fontSize:responsiveFontSize(7)}} >
            {data.description}
        </Text>
        </View>


      </ScrollView>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
})

const mapDispatchToProps = {
  
}

export default connect(mapToStateProps,mapDispatchToProps)(AddsDetails);