import { Navigation } from 'react-native-navigation';
import { Provider } from "react-redux";
import store from "../store";
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

import Chat from './Chat';
import Offers from './Offers';
import NotificationDetails from './NotificationDetails';
import BillDetails from './BillDetails';
import ClientOrderDetails from './ClientOrderDetails';
import SalesmanHome from './SalesmanHome';
import MyOrders from './MyOrders'
import CategoryProducts from './CategoryProducts'
import AddsDetails from './AddsDetails'
import Categories from './Categories'
import Adds from './Adds';
import MoreProductDetails from './MoreProductDetails';
import MenuContent from "../components/MenuContent";
import SplashScreen from '../screens/SplashScreen';

import About from './About';

import Login from '../screens/Login';
import Signup from '../screens/Signup';
import Home from '../screens/Home';
import TermsAndCondictions from '../screens/TermsAndCondictions';
import SelectLanguage from './SelectLanguage';
import Notifications from './Notifications';

import ProductDetails from './ProductDetails';
import Basket from './Basket';
import ChangePassword from './ChangePassword';
import Profile from './Profile';
import ForgetPassword from './ForgetPassword';
import NotificationDetailsRefusedOrder from './NotificationDetailsRefusedOrder';
import ChatPeople from './ChatPeople'
import DirectChat from './DirectChat'


export function registerScreens() {
     Navigation.registerComponent("DirectChat", () => DirectChat, store, Provider);
     Navigation.registerComponent("ChatPeople", () => ChatPeople, store, Provider);
     Navigation.registerComponent("Chat", () => Chat, store, Provider);
     Navigation.registerComponent("NotificationDetailsRefusedOrder", () => NotificationDetailsRefusedOrder, store, Provider);
     Navigation.registerComponent("NotificationDetails", () => NotificationDetails, store, Provider);
     Navigation.registerComponent("BillDetails", () => BillDetails, store, Provider);
     Navigation.registerComponent("ClientOrderDetails", () => ClientOrderDetails, store, Provider);
     Navigation.registerComponent("SalesmanHome", () => SalesmanHome, store, Provider);
     Navigation.registerComponent("MyOrders", () => MyOrders, store, Provider);
     Navigation.registerComponent("Offers", () => Offers, store, Provider);
     Navigation.registerComponent("AddsDetails", () => AddsDetails, store, Provider);
     Navigation.registerComponent("Adds", () => Adds, store, Provider);
     Navigation.registerComponent("CategoryProducts", () => CategoryProducts, store, Provider);
     Navigation.registerComponent("Categories", () => Categories, store, Provider);
     Navigation.registerComponent("MoreProductDetails", () => MoreProductDetails, store, Provider);
     Navigation.registerComponent("SplashScreen", () => SplashScreen, store, Provider);
     Navigation.registerComponent("About", () =>gestureHandlerRootHOC(About), store, Provider);
     Navigation.registerComponent("Login", () => gestureHandlerRootHOC(Login), store, Provider);
     Navigation.registerComponent("MenuContent", () => gestureHandlerRootHOC(MenuContent), store, Provider);
     Navigation.registerComponent("Signup", () => gestureHandlerRootHOC(Signup), store, Provider);
     Navigation.registerComponent("Home", () => gestureHandlerRootHOC(Home), store, Provider)
     Navigation.registerComponent("TermsAndCondictions", () => gestureHandlerRootHOC(TermsAndCondictions), store, Provider);  
     Navigation.registerComponent("SelectLanguage", () => gestureHandlerRootHOC(SelectLanguage), store, Provider);
     Navigation.registerComponent("Notifications", () => gestureHandlerRootHOC(Notifications), store, Provider);
     Navigation.registerComponent("Notifications", () => gestureHandlerRootHOC(Notifications), store, Provider);
     Navigation.registerComponent("ProductDetails", () => gestureHandlerRootHOC(ProductDetails), store, Provider);
     Navigation.registerComponent("Basket", () => gestureHandlerRootHOC(Basket), store, Provider);
     Navigation.registerComponent("ChangePassword", () => gestureHandlerRootHOC(ChangePassword), store, Provider);
     Navigation.registerComponent("Profile", () => gestureHandlerRootHOC(Profile), store, Provider);
     Navigation.registerComponent("ForgetPassword", () => gestureHandlerRootHOC(ForgetPassword), store, Provider);

    

    }

