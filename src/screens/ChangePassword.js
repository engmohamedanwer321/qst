import React,{Component} from 'react';
import {View,Keyboard} from 'react-native';
import {  moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Button} from 'native-base';
import * as colors from '../assets/colors'
import AppInput from '../common/AppInput';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import AppText from '../common/AppText';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import {BASE_END_POINT} from '../AppConfig';
import SnackBar from 'react-native-snackbar-component';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import LoadingOverlay from '../components/LoadingOverlay';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';



const MyButton =  withPreventDoubleClick(Button);


const validate = values => {
    const errors = {};

    const confirmPassword = values.confirmPassword
    const password = values.password;
    const oldPassword = values.oldPassword;

    if (oldPassword == null) {
        errors.oldPassword = Strings.require;
    }
    if (password == null) {
        errors.password = Strings.require;
    }
    if (confirmPassword == null) {
        errors.confirmPassword = Strings.require;
    }

    if( confirmPassword!= password){
        errors.confirmPassword = Strings.errorConfirmPassword
    }

    return errors;
};

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}


class ChangePassword extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    };

    state = {
        noConnection:null,
        loading:false,
    }
    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });     
        }
    componentDidMount(){    
        this.disableDrawer(); 
        console.log("USER TOKEN    "+this.props.user.firstname)
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    renderContent() {
        // const { navigator, isRTL } = this.props;
         return (
             <View>

                 <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="oldPassword" isRTL={this.props.isRTL} numeric label={Strings.oldPassword} component={InputComponent}
                     returnKeyType="done"
                     inputRef={el => this.passwordField = el }
                     onSubmit={() => {
                         Keyboard.dismiss()
                     }}
                />

                 <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="password"   isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.newPassword} component={InputComponent}
                  returnKeyType="done"
                     onSubmit={() => {
                        Keyboard.dismiss()
                     }}
                 />
 
                 <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="confirmPassword" isRTL={this.props.isRTL}  label={Strings.confirmPassword} component={InputComponent}
                     returnKeyType="done"
                     inputRef={el => this.passwordField = el }
                     onSubmit={() => {
                         Keyboard.dismiss()
                     }}
                     
                 />

                 
                
             </View>
 
         )
     }

     onChangePassword(values) {
         this.setState({loading:true}) 
         const data = {
            currentPassword:values.oldPassword,
            newPassword:values.password
         }    
         axios.put(`${BASE_END_POINT}user/updatePassword`, JSON.stringify(data) , {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          }).then(response=>{
              console.log('update user profile done')      
              console.log(response.data)
              this.setState({loading:false,noConnection:null});           
              RNToasty.Success({title:Strings.passwordChanges})
        
        }).catch(error=>{
            console.log(error.response)
            this.setState({loading:false})
            if(!error.response){
                this.setState({noConnection:Strings.noConnection})
            }
            
            if(error.response.status == 400){
                RNToasty.Error({title:Strings.oldPassError})
            }
        })
       
    }

    render(){
        const { handleSubmit,barColor } = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.changePassword} showBack navigator={this.props.navigator}/>
                <View style={{alignSelf:'center',width:responsiveWidth(80),marginTop:moderateScale(7)}}>
                    {this.renderContent()}
                    <MyButton 
                    onPress={handleSubmit(this.onChangePassword.bind(this))}
                    style={{marginTop:moderateScale(15), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:barColor,borderRadius:moderateScale(2.5)}}
                    >
                        <AppText fontSize={responsiveFontSize(3)} text={Strings.save} color='white' />
                    </MyButton>
                </View>
                {this.state.loading&&<LoadingDialogOverlay title={Strings.wait}/>}
            </View>
        )
    }
}

const form = reduxForm({
    form: "ChangePassword",
    validate,
})(ChangePassword)

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps)(form);

