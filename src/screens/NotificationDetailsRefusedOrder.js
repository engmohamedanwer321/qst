import React,{Component} from 'react';
import {
     View,Image,Text,TouchableOpacity,ScrollView,ActivityIndicator
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import {Input,Item,Label} from 'native-base';
import strings from '../assets/strings';
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';



class NotificationDetailsRefusedOrder extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    };

    state={
        firstBatch:0,
        months:0,
        load:false,
        getReasonLoading:true,
        reason:'',
        status:''
    }

    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });
          this.getReason()
        }
  
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    componentDidMount(){
        this.enableDrawer();
    }

    getReason = () => {
        //this.setState({getReasonLoading:true})
        axios.get(`${BASE_END_POINT}orders/${this.props.data.subject}`)
        .then(response=>{
            console.log('get Resons Done')
            console.log(response.data)
            this.setState({status:response.data.status, getReasonLoading:false,reason:response.data.reason})
        })
        .catch(error=>{
            this.setState({getReasonLoading:false})
            console.log('get Resons error')
            console.log(error.response)
        })
    }

    update = () => {
        this.setState({load:true})
        axios.put(`${BASE_END_POINT}orders/${this.props.data.subject}/accept`,JSON.stringify({
            monthCount:this.state.months,
	        firstPaid:this.state.firstBatch
        }),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`  
            },
          }).then(response=>{
                console.log(response.data);
                RNToasty.Success({title:strings.done})
                this.setState({load:false})
                this.props.navigator.pop()
          }).catch(error=>{
            console.log(error.response);
            this.setState({load:false})
            RNToasty.Error({title:strings.error})
          })
    }


    render(){
        const {data} = this.props;
        console.log("my data   ")
        console.log(data)
        return(
            <View style={{flex:1}}>               
                <AppHeader title={strings.reviwOrderInfo}   showBack navigator={this.props.navigator} />
                {this.state.getReasonLoading?
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator />
                </View>
                :
                <ScrollView style={{ flex: 1 }}>
                <Image resizeMode='contain' style={{marginVertical:moderateScale(10), alignSelf:'center', width:responsiveWidth(70),height:responsiveHeight(30)}} source={require('../assets/imgs/review.png')} />
                
                <View style={{alignSelf:'center',marginHorizontal:moderateScale(10),}}>
                   <Text>{data.description}</Text>
                </View>
                <View style={{alignSelf:'center',marginHorizontal:moderateScale(10),}}>
                   <Text>{this.state.reason}</Text>
                </View>

           
            <View style={{alignSelf:'center',justifyContent:'center',alignItems:'center',marginTop:moderateScale(5)}} >
         
            
            <View style={{width:responsiveWidth(70),marginVertical:moderateScale(2)}}>
             <Item floatingLabel>
               <Label>{strings.firstBatch}</Label>
               <Input 
               keyboardType='numeric'
               onChangeText={(value)=>{
                if(value){
                 this.setState({firstBatch:parseInt(value, 10)})
                }
             }}
               />
             </Item>
             </View>
 
             <View style={{width:responsiveWidth(70),marginVertical:moderateScale(5)}}>
             <Item floatingLabel>
               <Label>{strings.monthNumber}</Label>
               <Input 
               keyboardType='numeric'
               onChangeText={(value)=>{
                if(value){
                    this.setState({months:parseInt(value, 10)})
                   }
             }}
               />
             </Item>
             </View>

             </View>
            
             
             
             <TouchableOpacity
             onPress={()=>{
                 if(this.state.firstBatch&&this.state.months){
                     this.update()
                 }else{
                    RNToasty.Warn({title:strings.pleaseEnterMonthNumber})
                 }
             }}
              style={{alignSelf:'center',borderRadius:moderateScale(3), backgroundColor:this.props.barColor, width:responsiveWidth(70),height:responsiveHeight(8),justifyContent:'center',alignItems:'center',marginVertical:moderateScale(10)}}>
                 <Text style={{color:'white',fontSize:responsiveFontSize(7.5)}}>{strings.done}</Text>
             </TouchableOpacity>
             
             </ScrollView>
                }
             {this.state.load&&<LoadingDialogOverlay title={strings.wait}/>}
             
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser : state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps)(NotificationDetailsRefusedOrder);