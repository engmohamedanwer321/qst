import React,{Component} from 'react';
import {View,Image,AsyncStorage,Text} from 'react-native';
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import {Button,Icon} from 'native-base';
import Strings from '../assets/strings';
import AppText from '../common/AppText'
import AppHeader from '../common/AppHeader';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import ActionButton from 'react-native-action-button';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {  moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import {removeItem} from '../actions/MenuActions';

class SelectLanguage extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    };

    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }

    componentWillUnmount(){
        this.props.removeItem()
      }
     
    render(){
        const {isRTL,barColor} = this.props
        return(
            <View style={{flex:1}}>
            {!this.props.hideMenu&&<AppHeader navigator={this.props.navigator} showBurger  title={Strings.settings} /> }
                
                <View style={{elevation:10,backgroundColor:'white'}}>

                <View style={{alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start'}}>
                    <Text style={{color:colors.darkPrimaryColor,fontSize:responsiveFontSize(6),fontStyle:'italic',fontWeight:'600' }} >{Strings.selectLanguage}</Text>
                    <Icon name='language' type='Entypo' style={{marginHorizontal:moderateScale(3), color:'#6b8fdb',fontSize:responsiveFontSize(7)}}  />
                </View>
                
                <View style={{alignSelf:'center'}}>
                    <Button
                    onPress={()=>{
                        this.props.changeLanguage(false);
                        Strings.setLanguage('en');
                        AsyncStorage.setItem('@lang','en')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.navigator.pop()
                    }}
                     style={{backgroundColor:barColor,justifyContent:'center',alignItems:'center',width:wp(50),hp:hp(6.5),borderRadius:wp(10),marginTop:hp(5)}} >
                        <AppText fontSize={wp(5)} text='English' color='white'/>
                    </Button>

                    <Button
                     onPress={()=>{
                        this.props.changeLanguage(true);
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang','ar')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.navigator.pop()
                    }}
                    style={{marginVertical:hp(4), backgroundColor:barColor,justifyContent:'center',alignItems:'center',width:wp(50),hp:hp(6.5),borderRadius:wp(10)}} >
                    <AppText fontSize={wp(5)} text='العربية' color='white'/>
                    </Button>
                </View>

                </View>


                <View style={{elevation:10,backgroundColor:'white',marginVertical:hp(3)}}>
                            
                <View style={{alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start'}}>
                    <Text style={{color:colors.darkPrimaryColor,fontSize:responsiveFontSize(6),fontStyle:'italic',fontWeight:'600' }} >{Strings.selectColor}</Text>
                    <Icon name='ios-color-palette' type='Ionicons' style={{marginHorizontal:moderateScale(3), color:'#6b8fdb',fontSize:responsiveFontSize(7)}}  />
                </View>
                
                <View style={{alignSelf:'center', flexDirection:isRTL?'row-reverse':'row',marginVertical:moderateScale(2),marginBottom:moderateScale(5)}}>
                    <Button
                    onPress={()=>{
                        this.props.changeColor('#0c246b');
                        AsyncStorage.setItem('@color','#0c246b')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.navigator.pop()
                    }}
                     style={{backgroundColor:colors.darkPrimaryColor,width:hp(13),height:hp(13),borderRadius:hp(8),marginHorizontal:moderateScale(5)}} >
                       
                    </Button>

                    <Button
                    onPress={()=>{
                        this.props.changeColor('#353B48');
                        AsyncStorage.setItem('@color','#353B48')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.navigator.pop()
                    }}
                     style={{backgroundColor:'#353B48',width:hp(13),height:hp(13),borderRadius:hp(8),marginHorizontal:moderateScale(5)}} >
                       
                    </Button>

                    <Button
                    onPress={()=>{
                        this.props.changeColor('#D35400');
                        AsyncStorage.setItem('@color','#D35400')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.navigator.pop()
                    }}
                     style={{ backgroundColor:'#D35400',width:hp(13),height:hp(13),borderRadius:hp(8),marginHorizontal:moderateScale(5)}} >
                       
                    </Button>

                    


                </View>

                </View>

            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
   
})

const mapDispatchToProps = {
    changeLanguage,
    changeColor,
    removeItem
}


export default connect(mapToStateProps,mapDispatchToProps)(SelectLanguage);