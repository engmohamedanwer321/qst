import React,{Component} from 'react';
import {View,AsyncStorage,Image,AppState,Alert} from 'react-native';
import { moderateScale, responsiveWidth,  responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Button} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';

import ShoppingCard from '../components/ShoppingCard';
import {RemoveProductFromBacket,ClearBacket,putLocalOrder} from '../actions/OrderAction';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';

class Basket extends Component {

    state = {
        noConnection:null,
        orderLoading:false,
        myOrder:null,
        myOrderData:null,
    }
    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    }
    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
         // Alert.alert(this.props.orders.productOrders[0].count+"")
      }
    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });     
        this.renderLayoutProvider = new LayoutProvider(
          () => 2,
          (type, dim) => {
            dim.width = responsiveWidth(50);
            dim.height = responsiveHeight(51);
          },
        );
      }

    componentDidMount(){
        this.enableDrawer() 
        const {navigator,orders,ordersData} = this.props;
        AppState.addEventListener('change', (appState)=>{
            console.log('App has come to the foreground!  ',appState);
            if(appState == 'background'&&this.props.ordersData.length){
                const o_d = JSON.stringify(this.props.ordersData)
                const o = JSON.stringify(this.props.orders)
                AsyncStorage.setItem("orderData",o_d)
                AsyncStorage.setItem('order', o)
            }
        });
    }

    _handleAppStateChange = (nextAppState) => {
        console.log('App has come to the foreground!  ',nextAppState);
      };
    

    componentWillUnmount(){
        console.log('Leave')
        const {navigator,orders,ordersData} = this.props;
        if(this.props.ordersData.length){
        console.log("un Mount")
        const o_d = JSON.stringify(this.props.ordersData)
        const o = JSON.stringify(this.props.orders)
        AsyncStorage.setItem("orderData",o_d)
        AsyncStorage.setItem('order', o)
        }else{
            console.log("not unMount")
        }
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    totalPrice = () => {
        const {orders,ordersData} = this.props;
        console.log("mohamed anwer")
        console.log(orders.productOrders)
        total = 0;
        for(var i=0; i<orders.productOrders.length; i++){
            if(ordersData[i].hasOffer){
                total+= orders.productOrders[i].count*ordersData[i].offer ;
            }else{
                total+= orders.productOrders[i].count*ordersData[i].installmentPrice ;
            }
           
            console.log("enter")
            console.log(total)
        }
        return total;
    }
    renderRow = (type, data, row) => {
     return (
    <View style={{marginBottom:moderateScale(1), marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <ShoppingCard 
        navigator={this.props.navigator}
        removeProduct={()=>{
            this.props.RemoveProductFromBacket(data.id);
        }}
         count={this.props.orders.productOrders[row].count}
         months={this.props.orders.productOrders[row].monthCount}
         installment={this.props.orders.productOrders[row].costPerMonth} 
         data={data}
        />
    </View> 
     );
    }

    sendOrder = () => {
        //let userID = this.props.currentUser.user.id;
        let userToken = this.props.currentUser.token;
        console.log(this.props.orders)
        this.setState({orderLoading:true})
        axios.post(`${BASE_END_POINT}orders/${this.props.currentUser.user.id}/users`, JSON.stringify(this.props.orders), {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${userToken}`    
            },
          }).then(response=>{
            console.log(response)
           
            AsyncStorage.removeItem('order')
            .then(response=>{
                AsyncStorage.removeItem('orderData')
                .then(r=>{
                    RNToasty.Success({title:Strings.orderAddSuccess});
                    this.props.ClearBacket()
                    this.props.navigator.pop()
                    console.log("remove done")
                })
               
            }).catch(error =>{
                console.log("remove error")
                console.log(error)
            })
             /*AsyncStorage.multiRemove(['orderData','order'])
             .then(response=>{
                 console.log('remove')
             }).catch(error=>{
                console.log('remove error')
                console.log(error)
            })*/
              
              
              console.log(this.props.orders.productOrders)
              this.setState({orderLoading:false})

          }).catch(error=>{
            console.log('order Error')
           console.log(error.response)
            this.setState({orderLoading:false})
            if(!error.response){
                RNToasty.Error({title:Strings.noConnection});
            }
          });
    }
   
    render(){
        const {navigator,orders,ordersData,barColor} = this.props;
        console.log(orders)
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.shoppingBasketPage} showBack navigator={navigator}/>
                
                {
                    !ordersData.length ?
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode='contain' source={require('../assets/imgs/emptyBasket.png')} style={{width:responsiveWidth(90),height:responsiveHeight(50)}}  />
                    </View>
                    :
                    <View style={{flex:1}}>

                    <RecyclerListView 
                    extendedState={this.props}         
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(ordersData)}
                    rowRenderer={this.renderRow}                  
                />

                {ordersData.length>0&&
                <View style={{marginBottom:moderateScale(20),justifyContent:'center',alignItems:'center',flexDirection:this.props.isRTL?'row-reverse':'row', height:responsiveHeight(8),backgroundColor:'#CCCCCC'}}>
                    <AppText paddingHorizontal={moderateScale(2)} text={Strings.totalPrice} color={colors.primaryColor} fontSize={responsiveFontSize(8)} />
                    <AppText paddingHorizontal={moderateScale(2)} text={this.totalPrice()} color={colors.primaryColor} fontSize={responsiveFontSize(8)} />
                    <AppText text={Strings.pe} color={colors.primaryColor} fontSize={responsiveFontSize(8)} />
                </View>
                }

                {ordersData.length>0&&
                    <Button
                    onPress={()=>{
                        if(this.props.orders.productOrders.length==0){
                            RNToasty.Info({title:Strings.mustMakeOrder})
                        }else{
                           this.sendOrder();
                        }
                        
                    }}
                    style={{marginTop:moderateScale(6), width:responsiveWidth(100), justifyContent:'center',alignItems:'center',position: 'absolute',bottom:0, backgroundColor:barColor}}>
                       <AppText fontSize={responsiveFontSize(7)} color='white' text={Strings.orderNow} />
                   </Button>
                }


                    </View>
                }
                {this.state.orderLoading&&<LoadingDialogOverlay title={Strings.wait}/>}
                {this.props.logoutLoading&&<LoadingDialogOverlay title={Strings.waitLogout}/>}
                
            </View>
        );
    }
}



const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    orders: state.order.orders,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
   
})

const mapDispatchToProps = {
    RemoveProductFromBacket,
    ClearBacket,
    putLocalOrder
}

export default connect(mapStateToProps,mapDispatchToProps)(Basket);
