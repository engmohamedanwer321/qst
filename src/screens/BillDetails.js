import React,{Component} from 'react';
import {
    ActivityIndicator,TextInput, View,Text,TouchableOpacity,
    ScrollView,Platform,PermissionsAndroid
} from 'react-native';
import Permissions from 'react-native-permissions';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppHeader from '../common/AppHeader'
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import { Button, Icon,Badge,Input,Item,Label } from 'native-base';
import Swiper from 'react-native-swiper';
import {AddProductToBacket} from '../actions/OrderAction';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { RNToasty } from 'react-native-toasty';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import strings from '../assets/strings';


const MyButton =  withPreventDoubleClick(Button);


class BillDetails extends Component {
  state = {
    images: this.props.product.product.img,
    index:0,
    payLoading:false,
    showDialog:false,
    note:'',
  };

  static navigatorStyle = {
    navBarHidden: true,
   // statusBarColor: colors.darkPrimaryColor
  };
  constructor(props) {
    super(props);
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      }); 
  }
  componentDidUpdate(){
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      }); 
  }
  componentDidMount() {
    this.enableDrawer();
  }

  enableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

  
  renderSlideShow = () => {
    const {isRTL} = this.props;
    
    return(
        <View style={{marginBottom:moderateScale(6),width:responsiveWidth(100),height:responsiveHeight(30),justifyContent:'center',alignItems:'center',marginTop:moderateScale(8)}}>
            <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                <TouchableOpacity
                   onPress={()=>{
                    if(this.state.index==0){
                        this.setState({index:0})
                    }else{
                        this.setState({index:this.state.index-1})
                    }
                }} 
                >
                    <Icon name={isRTL? 'chevron-right':'chevron-left'} type='Entypo' style={{color:'#ff6f61'}} />
                </TouchableOpacity>
                <FastImage resizeMode='contain' style={{width:responsiveWidth(50),height:responsiveHeight(20), marginHorizontal:moderateScale(15)}} source={{uri:this.state.images[this.state.index]}} />
                <TouchableOpacity
                onPress={()=>{
                    if(this.state.index==this.state.images.length-1){
                        this.setState({index:0})
                    }else{
                        this.setState({index:this.state.index+1})
                    }
                }}
                >
                    <Icon name={isRTL? 'chevron-left':'chevron-right'} type='Entypo' style={{color:'#ff6f61'}} />
                </TouchableOpacity>
            </View>

            <View style={{marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
               
                {this.state.images.map((img,i)=>(
                     <TouchableOpacity
                     onPress={()=>{
                         this.setState({index:i})
                     }}
                     style={{borderColor:this.state.index==i? '#ff6f61' : '#CCCCCC', borderWidth:1, width:responsiveWidth(18),height:responsiveHeight(8), marginHorizontal:moderateScale(2),marginVertical:moderateScale(1.5)}}>
                        <FastImage resizeMode='stretch' style={{width:responsiveWidth(18),height:responsiveHeight(8),}} source={{uri:img}} />
                    </TouchableOpacity>
                ))}
                
            </View>
        </View>
    )
}

renderProductDetails = (type,value) =>{
    const {isRTL} = this.props
    return(
        <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',flexDirection:isRTL?'row-reverse':'row' , width:responsiveWidth(100),height:responsiveHeight(7),justifyContent:'space-between',alignItems:'center'}}>
            <AppText paddingHorizontal={moderateScale(9)} text={type} color='#06134C' fontSize={responsiveFontSize(6)} />
            <AppText paddingHorizontal={moderateScale(9)} text={value} color={colors.skipIconColor} fontSize={responsiveFontSize(6)} />
        </View>
    )
}
  
ClientPay = () => {
    this.setState({payLoading:true})
   axios.put(`${BASE_END_POINT}premiums/${this.props.product.id}/paid`,{},{
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.props.currentUser.token}`
    },
  }).then(response=>{
    this.setState({payLoading:false})
    RNToasty.Success({title:Strings.payDone})
        console.log("payed Done")
      console.log(response.data)
  }).catch(error=>{
    this.setState({payLoading:false})
    RNToasty.Success({title:Strings.payFail})
    console.log("payed error")
    console.log(error.response)
  })
}

ClientUnpay = () => {
    this.setState({payLoading:true,showDialog:false})
   axios.put(`${BASE_END_POINT}premiums/${this.props.product.id}/unpaid`,JSON.stringify({
       notes:this.state.note
   }),{
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.props.currentUser.token}`
    },
  }).then(response=>{
    this.setState({payLoading:false})
    RNToasty.Success({title:Strings.done})
        console.log("unpayed Done")
      console.log(response.data)
  }).catch(error=>{
    this.setState({payLoading:false})
    //RNToasty.Success({title:Strings.payFail})
    console.log("unpayed error")
    console.log(error.response)
  })
}
  
  
  render() {
    const { data, isRTL,fromHome,product,navigator,flag } = this.props;
    console.log("bill details")
    console.log(product)
    return (
      <View style={{ flex: 1 }}>
        <AppHeader  title={Strings.premiumDetails} showBack navigator={this.props.navigator} />                       
        <ScrollView>
            {!flag?
            <View>
            {this.renderProductDetails(Strings.productName,product.product.name)}
            {this.renderProductDetails(Strings.months,product.totalMonth)}
            {this.renderProductDetails(Strings.paidMonths,product.paidMonth)} 
            {this.renderProductDetails(Strings.remindingMonths,product.monthRemaining)}
            {this.renderProductDetails(Strings.upfront,product.firstPaid)}
            {this.renderProductDetails(Strings.paidCost,product.paidCost)}
            {this.renderProductDetails(Strings.reminingCost,product.costRemaining)} 
            {this.renderProductDetails(Strings.perMonth,product.costPerMonth)}
            </View>
            :
            <View>
            {this.renderProductDetails(Strings.productName,product.product.name)}
            {this.renderProductDetails(Strings.months,product.totalMonth)}
            {this.renderProductDetails(Strings.perMonth,product.costPerMonth)}
            {this.renderProductDetails(Strings.paidMonths,product.paidMonth)} 
            {this.renderProductDetails(Strings.remindingMonths,product.monthRemaining)}
            {this.renderProductDetails(Strings.paidCost,product.paidCost)}
            {this.renderProductDetails(Strings.reminingCost,product.costRemaining)}
            {this.renderProductDetails(Strings.salesmanName,product.salesMan.firstname+" "+product.salesMan.lastname)}
            {this.renderProductDetails(Strings.salesmanPhone,product.salesMan.phone[0])} 
          
            </View>
            }
            <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',width:responsiveWidth(100)}}></View>
        </ScrollView>

        {!flag&&
        <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',position:'absolute',bottom:0,left:0,right:0,width:responsiveWidth(100),height:responsiveHeight(8)}}>
        <TouchableOpacity
        onPress={()=>{
            this.ClientPay()
        }} 
        style={{justifyContent:'center',alignItems:'center',backgroundColor:'green',width:responsiveWidth(50),height:responsiveHeight(8)}} >
            <Text style={{color:'white',fontSize:responsiveFontSize(7)}} > {Strings.pay} </Text>
        </TouchableOpacity>

        <TouchableOpacity
        onPress={()=>{
            this.setState({showDialog:true})
        }} 
        style={{justifyContent:'center',alignItems:'center',backgroundColor:'red',width:responsiveWidth(50),height:responsiveHeight(8)}} >
            <Text style={{color:'white',fontSize:responsiveFontSize(7)}} > {Strings.unpay} </Text>
        </TouchableOpacity>
        </View>
        }
        {this.state.payLoading&&<LoadingDialogOverlay title={Strings.wait} />}

        <Dialog
            width={responsiveWidth(70)}
            height={responsiveHeight(40)}
            visible={this.state.showDialog}
            onTouchOutside={() => {
            this.setState({ showDialog: false });
            }}
           
            >
          
            <View style={{width:responsiveWidth(70),height:responsiveHeight(40)}}>
                <View
                style={{ width:responsiveWidth(55),alignSelf:'center',marginTop:moderateScale(15)}} 
                 >
                    <Item floatingLabel>
                    <Label 
                    >
                    {Strings.enterNotes}</Label>
                    <Input
                    style={{direction:isRTL?'rtl':'ltr'}}
                    onChangeText={(value)=>this.setState({note:value})}
                     />
                    </Item>
                </View>
                <TouchableOpacity
                onPress={()=>{
                   if(this.state.note){
                    console.log(this.state.note)
                    this.ClientUnpay()
                   }else{
                       RNToasty.Warn({title:Strings.enterNotes})
                   }
                }} 
                style={{marginTop:moderateScale(25),borderRadius:moderateScale(2), justifyContent:'center',alignItems:'center',backgroundColor:'green',width:responsiveWidth(50),height:responsiveHeight(8),alignSelf:'center'}} >
                    <Text style={{color:'white',fontSize:responsiveFontSize(7)}} > {Strings.send} </Text>
                </TouchableOpacity>
            </View>

            
        </Dialog>

      </View>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})

const mapDispatchToProps = {
  
}

export default connect(mapToStateProps,mapDispatchToProps)(BillDetails);