import React,{Component} from 'react';
import {
    ActivityIndicator,FlatList, View,Text,TouchableOpacity,
    ScrollView,Platform,ImageBackground
} from 'react-native';
import Permissions from 'react-native-permissions';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import LottieView from 'lottie-react-native';
import AppText from '../common/AppText';
import AppHeader from '../common/AppHeader'
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import { Button, Icon,Badge,Input,Item,Label,Thumbnail } from 'native-base';
import Swiper from 'react-native-swiper';
import {AddProductToBacket} from '../actions/OrderAction';
import { RNToasty } from 'react-native-toasty';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import strings from '../assets/strings';
import Communications from 'react-native-communications';
const MyButton =  withPreventDoubleClick(Button);


class ClientOrderDetails extends Component {
  state = {
    index:0,
    bills:[],
    loading:true,
  };

  static navigatorStyle = {
    navBarHidden: true,
    //statusBarColor: colors.darkPrimaryColor
  };

  getClientBills = () => {
   // const {clientID} = this.peops;
    this.setState({loading:true})
    axios.get(`${BASE_END_POINT}premiums/?client=${this.props.id}&salesMan=${this.props.currentUser.user.id}`)
    .then(response=>{
      this.setState({loading:false,bills:response.data.data})
      console.log('bills')
      console.log(response.data.data)
    })
    .catch(error=>{
      console.log('bills error')
      console.log(error.response)
    })
        
  }

  componentDidUpdate(){
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      }); 
  }
constructor(props) {
    super(props);
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      });     

}
  componentDidMount() {
    this.enableDrawer();
    this.getClientBills()
  }

  enableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

 
  
 renderClientHeaderInfo = () => {
    const {isRTL,data,barColor} = this.props;
    return(
       <View style={{backgroundColor:barColor,alignSelf:'center',width:responsiveWidth(100)}}>
        {/*<TouchableOpacity 
        onPress={()=>Communications.phonecall(data.phone[0], true)}
        style={{ marginTop:moderateScale(5),alignItems:isRTL?'flex-start':'flex-end',marginHorizontal:moderateScale(5)}}>
            <Icon style={{color:'white', fontSize:responsiveFontSize(2.5)}} name='phone' type='AntDesign' />
        </TouchableOpacity>
        */}
        <View style={{marginVertical:moderateScale(5),alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
         <Thumbnail large source={data.img?{uri:data.img}:require('../assets/imgs/profileicon.png')} />
         <Text style={{fontSize:responsiveFontSize(7),color:'white',marginTop:moderateScale(5)}}>{data.firstname} {data.lastname} </Text>        
         
         <View style={{marginTop:moderateScale(3), flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
        
         <TouchableOpacity
         style={{marginHorizontal:moderateScale(2), width:responsiveWidth(25),height:responsiveHeight(7),backgroundColor:'white',borderRadius:moderateScale(3),justifyContent:'center',alignItems:'center'}}
           onPress={()=>{
             console.log("my Data  ")
             console.log(data)
             this.props.navigator.push({
              screen: 'DirectChat',
              animated: true,
              passProps: {data:data}
          })
           }}
        >
           <Text style={{color:'black'}} >{strings.chat}</Text>
         </TouchableOpacity>
        
         {data.phone.map((num,index)=>(
           <TouchableOpacity
           onPress={()=>Communications.phonecall(num, true)}
           style={{ marginHorizontal:moderateScale(2), width:responsiveWidth(30),height:responsiveHeight(7),backgroundColor:'white',borderRadius:moderateScale(3),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',}}
           > 
            
             <Icon style={{marginHorizontal:moderateScale(2), color:'green', fontSize:responsiveFontSize(7)}} name='phone' type='AntDesign' />
             <Text style={{color:'black'}} >{num}</Text>
           </TouchableOpacity>
         ))}
        

         </View>
       
        </View>
        <View style={{alignItems:'center', marginVertical:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start',marginHorizontal:moderateScale(5),flexDirection:isRTL?'row-reverse':'row'}}>
            <Text style={{color:'gray', fontSize:responsiveFontSize(7)}}>{Strings.from}</Text>
            <Text style={{marginHorizontal:moderateScale(2), color:'white', fontSize:responsiveFontSize(6)}}>{data.address}</Text>
        </View>
       </View>

    )
}

renderDetails = (type,value) =>{
    const {isRTL} = this.props
    return(
        <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',flexDirection:isRTL?'row-reverse':'row' , width:responsiveWidth(100),height:responsiveHeight(7),justifyContent:'space-between',alignItems:'center'}}>
            <AppText paddingHorizontal={moderateScale(9)} text={type} color='#06134C' fontSize={responsiveFontSize(2.5)} />
            <AppText paddingHorizontal={moderateScale(9)} text={value} color={colors.skipIconColor} fontSize={responsiveFontSize(2)} />
        </View>
    )
}
  

  render() {
    const { data, isRTL,navigator } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <AppHeader  title={Strings.clientDetails} showBack navigator={this.props.navigator} />                   
        <ScrollView>
        {this.renderClientHeaderInfo()}   
       {this.state.loading?
       <View style={{ flex:1}}> 
       <LottieView
       style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
           source={require('../assets/animations/smartGarbageLoading.json')}
           autoPlay
           loop
           />
       </View>
       :
       <FlatList 
       numColumns={2}
       data={this.state.bills}
       renderItem={({item})=>(
         <MyButton
         onPress={()=>{
           navigator.push({
             screen: 'BillDetails',
             passProps:{product:item}
           })
         }}
          style={{height:responsiveHeight(20), padding:0, width:responsiveWidth(48),margin:moderateScale(2),alignSelf:'center'}}>
             <ImageBackground 
              source={require('../assets/imgs/clientDrawer.jpg')}
              style={{height:responsiveHeight(20),width:responsiveWidth(48),alignItems:'center',justifyContent:'center'}}>

               <Text style={{color:'black',fontSize:responsiveFontSize(6)}}>{item.product.name}</Text>
             </ImageBackground>
         </MyButton>
       )}
       />
       }
        
     </ScrollView>
      </View>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})

const mapDispatchToProps = {
  
}

export default connect(mapToStateProps,mapDispatchToProps)(ClientOrderDetails);