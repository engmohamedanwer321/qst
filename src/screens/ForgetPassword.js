import React, { Component } from "react";
import {
     View, AsyncStorage, StyleSheet, Dimensions, Keyboard,
     StatusBar, ImageBackground, KeyboardAvoidingView, NetInfo,
      TouchableOpacity, Platform, Alert, TextInput, ActivityIndicator
     } from "react-native";
import { Button,Item,Label,Input } from "native-base";
import { AppInput, AppText, AppHeader } from "../common";
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import { connect } from "react-redux"
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import allStrings from '../assets/strings';
import * as colors from '../assets/colors';
import StepIndicator from 'react-native-step-indicator';
import Swiper from "react-native-swiper";
import { API_ENDPOINT } from '../AppConfig';
const { width, height } = Dimensions.get('window');
import axios from 'axios'
import { BASE_END_POINT } from '../AppConfig';
import CodeInput from 'react-native-confirmation-code-input';
import { RNToasty } from 'react-native-toasty';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import SnackBar from 'react-native-snackbar-component';



const validate = values => {
    const errors = {};

    if (!values.phoneNumber) {
        errors.phoneNumber = allStrings.require;
    } /*else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = allStrings.errorStartPhone;
    }*/  else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = allStrings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = allStrings.errorPhoneFormat
    }

   /* if (!values.password) {
        errors.password = allStrings.require;
    }

    if (!values.confirmPassword) {
        errors.confirmPassword = allStrings.require;
    } else if (values.password != values.confirmPassword) {
        errors.confirmPassword = allStrings.errorConfirmPassword;
    }*/

    return errors;
};

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class ForgetPassword extends Component {
    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
   
           
    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
        rootNavigator = this.props.navigator;
    }

    static navigatorStyle = {
        navBarHidden: true,
       // statusBarColor: colors.darkPrimaryColor,
    };
    state = {
        currentPage: 0,
        loading: false,
        password: '',
        confirmPassword: true,
        passwordError:'',
        confirmPasswordError:'',
        noConnection:null,
        phoneNumber:null,
    }

    onInsertPhone(values) {
        
        this.setState({phoneNumber:values.phoneNumber, loading: true, })
        let body = { phone: values.phoneNumber }
        axios.post(`${BASE_END_POINT}forget-password`, JSON.stringify(body), {
            headers: { 'Content-Type': 'application/json', },
        }).then(response => {
            console.log('phone done')
            console.log(response.data)
          

            this.setState({
                currentPage: 1,
                loading: false,
                noConnection:null,
            })
            this.swiper.scrollBy(0);
        }).catch(error => {
            this.setState({loading:false});
            if (error.request.status === 400) {
                RNToasty.Error({title:allStrings.writeCorrectPhone})
            }
            if(!error.response){
                this.setState({noConnection:'allStrings.noConnection'});
            }
            console.log('phone error')
            console.log(error)
            console.log(error.response)
        });
        
    }
    
    
    renderInsertPhone() {
        const { handleSubmit,isRTL } = this.props;
        return (
            <View style={{flex: 1 }} >
                <View style={{alignSelf:'center', width:responsiveWidth(85)}}>
                    <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="phoneNumber" isRTL={this.props.isRTL} numeric marginBottom={moderateScale(3)} label={allStrings.mobileNumber} component={InputComponent}
                    returnKeyType="done"
                        onSubmit={() => {
                        Keyboard.dismiss()
                        }}
                    />
                </View>
                <View style={{ position: 'absolute', bottom: moderateScale(10), right:moderateScale(10), left:moderateScale(10),width:responsiveWidth(90)}}>
                   <View style={{alignSelf:isRTL?'flex-start':'flex-end'}}>
                    <TouchableOpacity                 
                        onPress={handleSubmit(this.onInsertPhone.bind(this))}
                        >
                            <AppText text={allStrings.next} color='black' fontSize={responsiveFontSize(4)} />
                     </TouchableOpacity>
                   </View>
                </View>
                <SnackBar                   
                   visible={this.state.noConnection!=null?true:false} 
                   textMessage={allStrings.noConnection}
                   messageColor='white'
                   backgroundColor={colors.primaryColor}
                   autoHidingTime={5000}/>
            </View>
        )
    }



    sendCode(code) {
       
        console.log(this.state.phoneNumber)
        console.log(code)
        this.setState({ loading: true, })
        let body = { phone: this.state.phoneNumber, verifycode: code }
        axios.post(`${BASE_END_POINT}confirm-code`, JSON.stringify(body), {
            headers: { 'Content-Type': 'application/json', },
        }).then(response => {
            this.setState({
                currentPage: 2, loading: false,
            })
            this.swiper.scrollBy(1);

        }).catch(error => {
            this.setState({ loading: false })
            if (error.request.status === 400) {              
                RNToasty.Error({title:allStrings.correctCode})
            }
            if(!error.response){
                this.setState({noConnection:'allStrings.noConnection'})
            }
            console.log('code error')
            console.log(error)
            console.log(error.response)
        });
    }
    _onFulfill(code) {
        this.sendCode(code);
    }

    renderInsertCode() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                <View style={styles.inputWrapper2}>
                    {/* 
                    <View style={{ alignSelf: 'center', marginHorizontal: 10, justifyContent: 'center', alignItems: 'center' }} >
                        <AppText textAlign="center" fontSize={20} color={colors.colorButtons} text={allStrings.enterCode + ' ' + this.state.phoneNumber + ' ' + allStrings.pleaseInsertThat} />
                         <Button transparent style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }} onPress={() =>{this.swiper.scrollBy(-1); this.setState({ currentPage: 0 })}}>
                            <AppText fontSize={16} text={allStrings.changeNumber} color={'black'} />
                        </Button> 
                    </View>
                    */}
                    
                    <CodeInput
                        ref="codeInputRef2"
                        secureTextEntry
                        keyboardType="numeric"
                        compareWithCode='1234'
                        activeColor={colors.primaryColor}//'rgba(49, 180, 4, 1)'
                        inactiveColor={colors.primaryColor}//'rgba(49, 180, 4, 1.3)'
                        autoFocus={false}
                        ignoreCase={true}
                        //maxLength={3}
                        codeLength={4}
                        
                        inputPosition='center'
                        size={50}
                        onFulfill={(isValid, code) => this._onFulfill(code)}
                        containerStyle={{ alignSelf: 'center', marginTop: 30 }}
                        codeInputStyle={{ borderWidth: 1.5 }}
                    />

                </View>
                <SnackBar                   
                   visible={this.state.noConnection!=null?true:false} 
                   textMessage={allStrings.noConnection}
                   messageColor='white'
                   backgroundColor={colors.primaryColor}
                   autoHidingTime={5000}/>
            </View>
        )
    }


    sendNewPassword() {
        this.setState({ loading: true, })
        let body = { phone: this.state.phoneNumber, newPassword: this.state.password }
        axios.post(`${BASE_END_POINT}reset-password`, JSON.stringify(body), {
            headers: { 'Content-Type': 'application/json', },
        }).then(response => {
            this.setState({ loading: false, })
            RNToasty.Success({title:allStrings.passwordCahngedSuccessfuly})
            this.props.navigator.pop()
        }).catch(error => {
            this.setState({ loading: false, })
            if(!error.response){
                this.setState({noConnection:'allStrings.noConnection'})
            }
            console.log('new Password error')
            console.log(error)
            console.log(error.response)
        });
    }

    requestNewPassword() {
         if(this.state.password.length==0){
            RNToasty.Error({ title: allStrings.enterPassword })
         }else if(this.state.confirmPassword.length==0){
            RNToasty.Error({ title: allStrings.enterConfirmPassword })
         }else if (this.state.password !== this.state.confirmPassword) {
            RNToasty.Error({ title: allStrings.errorConfirmPassword })
        } else {
            this.sendNewPassword();
        }
        
    }
    renderChangePassword = () => {
        const {isRTL, handleSubmit} = this.props;
        return (
            <View style={{ flex: 1 }} >
                <View style={{alignSelf:'center', width:responsiveWidth(85)}}>
                    
                        <Item floatingLabel>
                            <Label>{allStrings.password}</Label>
                            <Input onChangeText={(value)=>{
                            this.setState({password:value})
                            }}
                            />
                        </Item>
                       
                    
                    <Item style={{marginTop:20}} floatingLabel>
                        <Label>{allStrings.confirmPassword}</Label>
                        <Input onChangeText={(value)=>{
                           this.setState({confirmPassword:value})
                        }}
                        />
                    </Item> 

                </View>

                <View style={{ position: 'absolute', bottom: moderateScale(10), right:moderateScale(10), left:moderateScale(10),width:responsiveWidth(90)}}>
                   <View style={{alignSelf:isRTL?'flex-start':'flex-end'}}>
                    <TouchableOpacity                 
                        onPress={()=>{
                            this.requestNewPassword()
                        }}
                        >
                            <AppText text={allStrings.next} color={colors.skipIconColor} fontSize={responsiveFontSize(4)} />
                        </TouchableOpacity>
                   </View>
                </View>
                <SnackBar                   
                   visible={this.state.noConnection!=null?true:false} 
                   textMessage={allStrings.noConnection}
                   messageColor='white'
                   backgroundColor={colors.primaryColor}
                   autoHidingTime={5000}/>
            </View>
        )
    }
    swiper = null;

    render() {
        const { navigator } = this.props
        return (
            <View style={{flex:1}}>
                <AppHeader navigator={navigator} title={allStrings.forgetPassword} showBack />
                <ImageBackground source={require("../assets/imgs/boodyCarBackground.png")}
                    style={{ width: "100%", height: "100%" }}>
 

                    <View style={styles.stepIndicator}>
                        <StepIndicator
                         onPress={(position)=>{
                             //this.setState({currentPage:position});
                             //Alert.alert(2+"");
                         }}
                         stepCount={3} 
                         customStyles={firstIndicatorStyles}
                         currentPosition={this.state.currentPage}
                         labels={[allStrings.insertPhone, allStrings.insertCode, allStrings.insertNewPassword]} />
                    </View>
                   
                    <View style={styles.swiper}>
                        <Swiper
                            style={{flex:1}}
                            loop={false}
                            autoplay={false}
                            scrollEnabled={false}
                            showsButtons={false}
                            showsPagination={false}
                            ref={(s) => this.swiper = s}
                            index={this.state.currentPage}                            
                        >
                        
                            {this.renderInsertPhone()}
                            {this.renderInsertCode()}
                            {this.renderChangePassword()}
                            
                            
                        </Swiper>
                       
                    </View>
                    
                    {this.state.loading&& <LoadingDialogOverlay title={allStrings.wait} />}
                   
                   
                
                </ImageBackground>
            </View>
        )
    }
}

const firstIndicatorStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: colors.primaryColor,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: colors.primaryColor,
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: colors.primaryColor,
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: colors.primaryColor,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: colors.primaryColor,
  stepIndicatorLabelFinishedColor: 'white',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#cbc4bf',
    labelSize: 17,
    currentStepLabelColor: colors.primaryColor,
    labelFontFamily: Platform.OS == 'ios' ? 'Droid Arabic Kufi' : 'droidkufi',
}

const styles = StyleSheet.create({
    swiper: {
        height: "68.5%",
        // flex:1,
        paddingTop: moderateScale(5),
        width: '100%',
        // paddingHorizontal: moderateScale(5),
    },
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    stepIndicator: {
        marginVertical: height * 0.05,
    },
    page: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    customSwiperDot: {
        width: responsiveWidth(4),
        height: responsiveWidth(4),
        borderRadius: responsiveWidth(2),
    },
    spinnerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        flex: 1,
    },

    inputWrapper2: {
        justifyContent: 'center',
        alignItems: 'center',
    }
});

const form = reduxForm({
    form: "ForgetPassword",
    validate,
})(ForgetPassword)

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color 
}
)

export default connect(mapToStateProps)(form);