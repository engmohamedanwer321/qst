import React,{Component} from 'react';
import {
     View,Image,TouchableOpacity,Linking,Text
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import strings from '../assets/strings';
import {removeItem} from '../actions/MenuActions';





class About extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,     
          });  
        }
        componentDidUpdate(){
            this.props.navigator.setStyle({
                statusBarColor: this.props.barColor,
                
              }); 
          }
    
  
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    componentDidMount(){
        this.enableDrawer();
    }

    componentWillUnmount(){
        this.props.removeItem()
      }


    render(){
        const {isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                
                <AppHeader title={strings.aboutUS}   showBurger navigator={this.props.navigator} />
                
                <Image style={{alignSelf:'center',marginTop:moderateScale(20), width:responsiveWidth(80),height:responsiveHeight(25)}} source={require('../assets/imgs/mtqst.png')} />
                
                <View style={{alignSelf:'center',marginTop:moderateScale(1)}}>
                    <AppText color='gray' text={strings.version} />
                </View>
               
                <View style={{marginHorizontal:moderateScale(2), alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(20)}}>
                    <View style={{alignSelf:isRTL?'flex-end':'flex-start'}}>
                    <AppText color={colors.buttonColor} fontWeight='500' text={strings.qsatha} fontSize={responsiveFontSize(7)} />
                    </View>
                        
                    <AppText padding={moderateScale(5)} color='gray'  text='شركة قسط شركة رائدة فى بيع المنتجات بنظام التقسيط واهم ما يميزنا باننا نمنحك القدرة على تحديد مدة التقسيط بالاضافة الى العروض المميزة التى تتيحها الشركة' fontSize={responsiveFontSize(6)} />
                </View>
               
                <TouchableOpacity 
                onPress={()=>{
                    Linking.openURL('http://hashsc.com/').catch((err) => console.error('An error occurred', err));
                }}
                style={{backgroundColor:'#f2efef', flexDirection:isRTL?'row-reverse':'row', width:responsiveWidth(100),height:responsiveHeight(7),justifyContent:'center',alignItems:'center',position:'absolute',bottom:0,right:0,left:0}}>
                    <AppText color='black'  text={strings.thisAppDesigned} fontSize={responsiveFontSize(6)} />
                    <AppText color={colors.buttonColor} fontWeight='600'  text={strings.hash} fontSize={responsiveFontSize(6.5)} />
                    <Image style={{marginHorizontal:moderateScale(3), width:responsiveWidth(11),height:responsiveHeight(6)}} source={require('../assets/imgs/hash.png')} />
                </TouchableOpacity>

            
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color 
})
const mapDispatchToProps = {
    removeItem
 }

export default connect(mapToStateProps,mapDispatchToProps)(About);