import React, { Component } from 'react';
import {
  View,Keyboard,ScrollView,AsyncStorage,TouchableOpacity,Text
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';



const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.cardNum) {
        errors.cardNum = Strings.require;
    }

    if (!values.address) {
        errors.address = Strings.require;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } /*else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = Strings.errorStartPhone;
    }  else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }*/
    
    return errors;
};
;

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class Profile extends Component {

    state = {
        userImage:null,
        noConnection:null,
        updateLoading:false,
        token:this.props.currentUser.token,
        selectedCity:this.props.currentUser.user.area
    }

    

   
    static navigatorStyle = {
        navBarHidden: true,
       // statusBarColor: colors.darkPrimaryColor,
    };

    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });
          
         
      }

    constructor(props) {
        super(props);
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
        rootNavigator = this.props.navigator; 
    }
    
    componentDidMount(){    
        this.disableDrawer();
       // const visibleScreenInstanceId = await this.props.navigator.screenInstanceID
        //console.log("Screen id",visibleScreenInstanceId)
    }

    componentWillUnmount(){
        this.props.removeItem()
      }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

   
    
    onUpdate(values) {
        var data = new FormData(); 
            data.append('firstname',values.firstName);
            data.append('lastname',values.lastName)
            data.append('phone',values.phoneNumber)
            data.append('email',values.email)
            data.append('address',values.address)
            data.append('language','arabic')
            data.append('cardNum',values.cardNum)
            
        if(this.state.userImage!=null){
            data.append('img',{
                uri: this.state.userImage,
                type: 'multipart/form-data',
                name: 'productImages'
            }) 
        } 
        console.log('new user') 
        //console.log(userData)
       this.setState({updateLoading:true})
        axios.put(`${BASE_END_POINT}user/updateInfo`, data, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
            console.log('update user profile done')      
            console.log(response.data)
            this.setState({updateLoading:false,noConnection:null});           
            const user = {...this.props.currentUser,user:{...response.data.user}}
            console.log('current user')
            console.log(user)
            RNToasty.Success({title:Strings.updateProfileSuccess})
            this.props.getUser(user)
            AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
            /*this.props.navigator.push({
                screen: 'Home',
                animated: true,
            })*/
        }).catch(error => {
            console.log('error');
            console.log(error.response);
            console.log(error);
            this.setState({updateLoading:false});
             
            if (!error.response) {
            this.setState({noConnection:Strings.noConnection});
            } 
          });
    }

    renderCitesPicker = () => {
        console.log(this.state.companies);
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(80),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
             <Label style={{fontSize:responsiveFontSize(7),color:'gray',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.city}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.companyLoad?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker 
                     enabled={this.props.currentUser.user.type=='SALES-MAN'?false:true}          
                     mode="dropdown"
                     iosHeader="Select your Company"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedCity}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedCity:value});
                     }}
                     >
                     <Picker.Item key='cairo' label={Strings.cairo} value='cairo'/>
                     <Picker.Item key='ismailia' label={Strings.ismailia} value='ismailia'/>
                     <Picker.Item key='alexandria' label={Strings.alexandria} value='alexandria'/>
                     <Picker.Item key='giza' label={Strings.giza} value='giza'/>
                     <Picker.Item key='aswan' label={Strings.aswan} value='aswan'/>
                     <Picker.Item key='laxour' label={Strings.laxour} value='laxour'/>
                     <Picker.Item key='asyut' label={Strings.asyut} value='asyut'/>
                     <Picker.Item key='beheira' label={Strings.beheira} value='beheira'/>
                     <Picker.Item key='mansoura' label={Strings.mansoura} value='mansoura'/>
                     <Picker.Item key='damietta' label={Strings.damietta} value='damietta'/>
                     <Picker.Item key='faiyum' label={Strings.faiyum} value='faiyum'/>
                     <Picker.Item key='tanta' label={Strings.tanta} value='tanta'/>
                     <Picker.Item key='kafrElSheikh' label={Strings.kafrElSheikh} value='kafrElSheikh'/>
                     <Picker.Item key='minya' label={Strings.minya} value='minya'/>
                     <Picker.Item key='shibinElKom' label={Strings.shibinElKom} value='shibinElKom'/>
                     <Picker.Item key='arish' label={Strings.arish} value='arish'/>
                     <Picker.Item key='portSaid' label={Strings.portSaid} value='portSaid'/>
                     <Picker.Item key='banha' label={Strings.banha} value='banha'/>
                     <Picker.Item key='qena' label={Strings.qena} value='qena'/>
                     <Picker.Item key='zagazig' label={Strings.zagazig} value='zagazig'/>
                     <Picker.Item key='suez' label={Strings.suez} value='suez'/>
                     <Picker.Item key='sohag' label={Strings.sohag} value='sohag'/>
                     <Picker.Item key='elTor' label={Strings.elTor} value='elTor'/>
    
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }
    
  
renderContent() {
    const { isRTL,currentUser } = this.props;
    return (
        <View>

            <View style={{width:wp(80), flexDirection:isRTL?'row-reverse':'row'}}>
                <View style={{width:wp(39)}}>
                    <Field borderColor='gray' style={{ width: wp(80) }} textColor={colors.primaryColor} name="firstName" isRTL={this.props.isRTL}  marginBottom={hp(1)} label={Strings.firstName} component={InputComponent}
                    returnKeyType="done"
                    editable={currentUser.user.type=='SALES-MAN'?false:true}
                        onSubmit={() => {
                            Keyboard.dismiss();
                        }}
                    />
                </View>

                <View style={{marginHorizontal:wp(2), width:wp(39)}}>
                    <Field borderColor='gray' style={{ width: wp(80) }} textColor={colors.primaryColor} name="lastName" isRTL={this.props.isRTL}  marginBottom={hp(1)} label={Strings.secondName} component={InputComponent} 
                       editable={currentUser.user.type=='SALES-MAN'?false:true}
                       returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss();
                        }}
                    />
                </View>
            </View>

            <Field borderColor='gray'  style={{ width: wp(80) }} textColor={colors.primaryColor} numeric name="phoneNumber" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.mobileNumber} component={InputComponent} 
                returnKeyType="done"
                editable={currentUser.user.type=='SALES-MAN'?false:true}
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />


            <Field borderColor='gray'  style={{ width: wp(80) }} textColor={colors.primaryColor} email name="email" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent} 
               returnKeyType="done"
               editable={currentUser.user.type=='SALES-MAN'?false:true}
               onSubmit={() => {
                   Keyboard.dismiss();
               }}
            />

            <Field borderColor='gray' numeric  style={{ width: wp(80) }} textColor={colors.primaryColor}   name="cardNum" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.cardNumber} component={InputComponent} 
                returnKeyType="done"
                editable={currentUser.user.type=='SALES-MAN'?false:true}
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />

            <Field borderColor='gray'  style={{ width: wp(80) }} textColor={colors.primaryColor}   name="address" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.address} component={InputComponent} 
                returnKeyType="done"
                editable={currentUser.user.type=='SALES-MAN'?false:true}
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />

            {this.renderCitesPicker()}
    
        </View>

    )
}

leftComponent = () => (
    <View style={{flexDirection:this.props.isRTL?'row-reverse':'row'}}>
        <TouchableOpacity
        style={{marginHorizontal:moderateScale(7)}}
        onPress={()=>{
            if(this.props.currentUser){
                this.props.navigator.push({
                    screen:'ChangePassword',
                    animated:true,
                    passProps:{user:this.props.currentUser.user}
                  })
            }else{
                RNToasty.Warn({title:strings.checkUser}) 
            }
        }}
        >
            {/*<Icon style={{color:'white',fontSize:20}} type='FontAwesome5' name='unlock' />*/}
            <Text style={{color:'white'}}>{Strings.changePassword}</Text>
        </TouchableOpacity>        
    </View>
)
    render(){
        const {currentUser,handleSubmit} = this.props;
       
        return(
            <View style={{ flex:1 }}>
                <AppHeader leftComponent={currentUser.user.type=='CLIENT'&&this.leftComponent()} showBurger navigator={this.props.navigator} title={Strings.profile} /> 
                <ScrollView style={{ flex:1 }} > 
                 
                 {currentUser.user.type=='CLIENT'&&
                 <TouchableOpacity 
                 onPress={handleSubmit(this.onUpdate.bind(this))}
                 style={{marginHorizontal:wp(5),marginTop:hp(3),alignSelf:this.props.isRTL?'flex-start':'flex-end'}}>
                     <Icon style={{color:colors.darkPrimaryColor,fontSize:wp(8)}} type='MaterialCommunityIcons' name='account-check' />
                 </TouchableOpacity>
                 }
                 <TouchableOpacity
                 onPress={()=>{
                    ImagePicker.openPicker({
                        width: 300,
                        height: 400,
                        cropping: true
                      }).then(image => {
                        this.setState({userImage:image.path})
                      });
                 }}
                  style={{marginTop:moderateScale(8), alignSelf:'center'}}>
                    <Thumbnail
                     large
                     source={this.state.userImage?{uri:this.state.userImage}:currentUser.user.img?{uri:currentUser.user.img}:this.state.userImage!=null?{uri:this.state.userImage}:require('../assets/imgs/profileicon.png')}
                    />
                 </TouchableOpacity> 

                <View style={{justifyContent:'center',alignItems:'center', flex:1, marginTop:moderateScale(10),width:responsiveWidth(100)}}>
                    <View style={{width:responsiveWidth(85)}}>
                        {this.renderContent()}
                    </View>
                </View>    
                </ScrollView>
                {this.state.updateLoading && <LoadingDialogOverlay title={Strings.waitUpdateProfile}/>}

               
               
               
            </View>
            
        );
    }
}



const form = reduxForm({
    form: "UpdateProfile",
    enableReinitialize:true,
    validate,
})(Profile)

const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    initialValues: {
        firstName: state.auth.currentUser.user.firstname,
        lastName: state.auth.currentUser.user.lastname,
        email: state.auth.currentUser.user.email,
        phoneNumber: state.auth.currentUser.user.phone[0],
        cardNum: ""+state.auth.currentUser.user.cardNum,
        address: state.auth.currentUser.user.address,
      },
      barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

