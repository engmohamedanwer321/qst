import React,{Component} from 'react';
import {
    Text,View,TouchableOpacity,ScrollView,Keyboard
} from 'react-native';
import Checkbox from 'react-native-custom-checkbox';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {  Icon,Item,Label ,Radio, Input} from 'native-base';
import {AddProductToBacket} from '../actions/OrderAction';
import { RNToasty } from 'react-native-toasty';
import strings from '../assets/strings';



class MoreProductDetails extends Component {
  state = {
    count:1,
    cach:false,
    installment:false,
    pricePerMonth:0,
    firstMonth:0,
    months:0,
    flag:false,
    firstBatch:0,
    m:0,
  };

  static navigatorStyle = {
    navBarHidden: true,
    //statusBarColor: colors.darkPrimaryColor
  };

  componentDidUpdate(){
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      }); 
  }
constructor(props) {
    super(props);
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      });
    }
  enableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

  
  renderProductCount  =  () => {
    
      const  {product,isRTL,offer} = this.props;
      console.log(product)
      return(
        <View>
        <View style={{ alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start'}}>
            <Text style={{color:colors.darkPrimaryColor,fontSize:responsiveFontSize(6),fontStyle:'italic',fontWeight:'600' }} >{strings.count}</Text>
            <Icon name='counter' type='MaterialCommunityIcons' style={{marginHorizontal:moderateScale(3), color:'#6b8fdb',fontSize:responsiveFontSize(7)}}  />
        </View>
            <View style={{alignSelf:'center',justifyContent:'center',alignItems:'center',flexDirection:this.props.isRTL?'row-reverse':'row', height:responsiveHeight(15),backgroundColor:'#F7F7F7'}}>
                    <View>
                        <TouchableOpacity 
                        onPress={()=>{
                            if(this.state.count!=1){
                                this.setState({count:this.state.count-1,firstBatch:'',m:'',flag:false})
                               
                            }             
                        }}
                        style={{borderRadius:moderateScale(2), backgroundColor:'#F2414E',justifyContent:'center',alignItems:'center',width:responsiveWidth(11),height:responsiveHeight(7)}}>
                            <Icon name='minus' type='MaterialCommunityIcons' style={{color:'white'}} />
                        </TouchableOpacity>
                    </View >
                    <View style={{marginHorizontal:moderateScale(10)}}>
                        <AppText text={this.state.count}color='#06134C' fontSize={responsiveFontSize(8)} />
                    </View>
                    <View>
                        <TouchableOpacity
                        onPress={()=>{
                            if(this.state.count<product.quantity){
                                this.setState({count:this.state.count+1,firstBatch:'',m:'',flag:false})  
                                   
                            }
                                
                        }}
                         style={{borderRadius:moderateScale(2),backgroundColor:'#82C141',justifyContent:'center',alignItems:'center',width:responsiveWidth(11),height:responsiveHeight(7)}}>
                            <Icon name='plus' type='MaterialCommunityIcons' style={{color:'white'}} />
                        </TouchableOpacity>
                    </View>
                 </View>
            </View>     
      )
  }

  rendertProductPayment = () => {
    const  {product,isRTL} = this.props;
    return(
      <View>
        <View style={{alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),alignSelf:isRTL?'flex-end':'flex-start'}}>
            <Text style={{color:colors.darkPrimaryColor,fontSize:responsiveFontSize(2.5),fontStyle:'italic',fontWeight:'600' }} >{strings.paymentSystem}</Text>
            <Icon name='money' type='FontAwesome' style={{marginHorizontal:moderateScale(3), color:'#6b8fdb',fontSize:responsiveFontSize(2)}}  />
        </View>
        <View style={{justifyContent:'space-between', alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),marginHorizontal:moderateScale(20)}}>
            <Text>{Strings.cach}</Text>
            <Checkbox
            checked={this.state.cach}
            style={{backgroundColor: "white",color: colors.darkPrimaryColor,borderRadius: 5}}
            onChange={(name, checked) => {
              this.setState({ cach: checked,installment:false });
            }}
          />  
        </View>
        {this.state.cach&&
        <View style={{alignSelf:'center',marginVertical:moderateScale(5)}} >
            <Text style={{color:colors.darkPrimaryColor}}>
                {product.cashPrice} {Strings.pe}
            </Text>
        </View>
        }
        <View style={{justifyContent:'space-between', alignItems:'center', flexDirection:isRTL? 'row-reverse':'row', margin:moderateScale(5),marginHorizontal:moderateScale(20)}}>
            <Text>{Strings.installment}</Text>
            <Checkbox
            checked={this.state.installment}
            style={{backgroundColor: "white",color: colors.darkPrimaryColor,borderRadius: 5}}
            onChange={(name, checked) => {
              this.setState({ installment: checked,cach:false });
            }}
          />
        </View>
        {this.state.installment&&
        <View style={{alignSelf:'center',marginVertical:moderateScale(5)}} >
            <Text style={{color:colors.darkPrimaryColor}}>
                {product.installmentPrice} {Strings.pe}
            </Text>
        </View>
        }
      </View>
      )
  }

  

  
  
  render() {
    const { isRTL,product,offer,barColor } = this.props;
    //Alert.alert(offer+"")
    return (
      <View style={{ flex: 1 }}>
        <AppHeader  title={product.name} showBack navigator={this.props.navigator} />                   
        {/*this.rendertProductPayment()*/}

        <ScrollView style={{ flex: 1 }}>
        {this.renderProductCount()}
       
        <View style={{alignSelf:'center',justifyContent:'center',alignItems:'center'}} >
         
        <View style={{width:responsiveWidth(70),marginVertical:moderateScale(10)}}>
            <Item floatingLabel>
              <Label>{strings.firstBatch}</Label>
              <Input 
              value={this.state.firstBatch}
              ref={ref=>this.firstBatchInput=ref}
              keyboardType='numeric'
              onChangeText={(value)=>{
               if(value){
                this.setState({firstBatch:parseInt(value, 10)})
                if(this.state.m){
                let p = offer? (product.offer*this.state.count) : (product.installmentPrice*this.state.count)  
                let price = p - value;
                let months =  parseInt(this.state.m, 10);
                let rem = price % months;
                let newPrice =  price - rem;
                let costPerMonth = newPrice / months;
                let firstMont = value+rem;
                this.setState({
                    flag:true,
                    months: months,
                    firstMonth:firstMont,
                    pricePerMonth:costPerMonth
                })
              }
               }
            }}
              />
            </Item>
            </View>

            <View style={{width:responsiveWidth(70),marginVertical:moderateScale(10)}}>
            <Item floatingLabel>
              <Label>{strings.monthNumber}</Label>
              <Input
              
              ref={ref=>this.monthInput=ref}
              keyboardType='numeric'
              onChangeText={(value)=>{
                const {data} = this.props;
                if(value && parseInt(value, 10)!=0){
                this.setState({m:value})  
                let p = offer? (product.offer*this.state.count) : (product.installmentPrice*this.state.count)  
                let price = p - this.state.firstBatch;
                let months =  parseInt(value, 10);
                let rem = price % months;
                let newPrice =  price - rem;
                let costPerMonth = newPrice / months;
                let firstMont = this.state.firstBatch+rem;
                this.setState({
                    flag:true,
                    months: months,
                    firstMonth:firstMont,
                    pricePerMonth:costPerMonth
                })
                }else{
                    this.setState({flag:false,})
                }
            }}
              />
            </Item>
            </View>
           
           {this.state.flag&&
           <View>
                <View  style={{marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}} >
                    <Text>{this.state.firstMonth}</Text>
                    <Text style={{marginHorizontal:moderateScale(3), color:'red',fontSize:responsiveFontSize(7)}} >{strings.batch}</Text>
                </View>

                <View  style={{marginVertical:moderateScale(6),justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}} >
                    <Text>{this.state.pricePerMonth}</Text>
                    <Text style={{marginHorizontal:moderateScale(3),color:'red',fontSize:responsiveFontSize(7)}} >{strings.perMonth}</Text>
                </View>
           </View>
           
           }
        </View>

        </ScrollView>

        <TouchableOpacity
        onPress={()=>{
            if(this.state.flag&&this.state.firstBatch){
                this.props.AddProductToBacket({
                    product:product.id,
                    count:this.state.count,
                    paymentSystem:"installment",
                    firstPaid:this.state.firstMonth,
                    monthCount:this.state.months,
                    costPerMonth:this.state.pricePerMonth
                },product)
                RNToasty.Success({title:Strings.addProductSuccessfuly})
                this.props.navigator. pop()
            }else{
                RNToasty.Warn({title:Strings.pleaseEnterMonthNumber})
            }
        }}
         style={{marginVertical:moderateScale(8), borderRadius:moderateScale(2),elevation:10, width:responsiveWidth(80),height:responsiveHeight(8), alignSelf:'center',justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',backgroundColor:barColor}} >
            <Icon name='shoppingcart' type='AntDesign' style={{color:'white'}} />
            <Text style={{marginHorizontal:moderateScale(5),color:'white',fontSize:responsiveFontSize(6.5)}}>
                {Strings.shoppingBasket}
            </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
})

const mapDispatchToProps = {
    AddProductToBacket,
}

export default connect(mapToStateProps,mapDispatchToProps)(MoreProductDetails);