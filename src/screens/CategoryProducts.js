import React,{Component} from 'react';
import {View,RefreshControl,NetInfo,Text,TextInput} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Picker} from 'native-base';
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import ProductCard from '../components/ProductCard'


const types=[false, 'up', 'down'];

class CategoryProducts extends Component {

    page=1;
    list=[];
    state= {
        index:0,
        errorText:null,
        allCategories: new DataProvider(),
        searchProducts: new DataProvider(),
        refresh:false,
        loading:true,
        pages:null,
        sortType: Strings.random,
        flag:0
    }

    static navigatorStyle = {
        navBarHidden: true,
       // statusBarColor: colors.darkPrimaryColor,
    };
    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }
    constructor(props) {
        super(props);    
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          });
        this.renderLayoutProvider = new LayoutProvider(
          () => 2,
          (type, dim) => {
            dim.width = responsiveWidth(50);
            dim.height = responsiveHeight(46);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getProducts(this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getProducts(this.page,true);
                }
            }
          );      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    getProducts(page, refresh,sortType) {
      
            //this.setState({loading:true})
            let uri;
            if(sortType){
                 uri = `${BASE_END_POINT}products/?categoryId=${this.props.categoryID}&salesRatio&sort=${sortType}&visible=true&page=${page}&limit=20`
            }else{
                 uri = `${BASE_END_POINT}products/?categoryId=${this.props.categoryID}&salesRatio&visible=true&page=${page}&limit=20`
            }
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false,loading:true})
            }
            axios.get(uri)
                .then(response => {
                    console.log("PRODUCTS   ")
                    console.log(response.data.data)
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        allCategories: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.allCategories.getAllData(), ...response.data.data]),
                    })
                    if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error.response);
                    console.log(error);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <ProductCard 
       data={data}
        onPress={()=>{
            this.props.navigator.push({
                screen:'ProductDetails',
                animated:true,
                passProps: {productData:data}
            })
        }}
       data={data}
       navigator={this.props.navigator}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,categoryName,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={categoryName} showBack navigator={navigator}/>
                {this.state.loading?
                <View style={{ flex:1}}> 
                <LottieView
                style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                source={require('../assets/animations/smartGarbageLoading.json')}
                autoPlay
                loop
                />
                 <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                <View style={{flex:1}} >
                    <View style={{elevation:4,zIndex:100,flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(100),justifyContent:'space-between',alignItems:'center',alignSelf:'center',backgroundColor:this.props.barColor}}>
                       <TextInput
                        onChangeText={(value) => {
                            //console.log(this.state.products._data)
                            //Alert.alert('dd')
                            this.setState({flag:value.length})
                            this.list = this.state.allCategories._data.filter((product)=>{
                              if(value.length>0){
                                if(product.name.toLowerCase().includes(value.toLocaleLowerCase())){
                                    console.log('include')
                                    return product
                                }
                              }
                            })
                            console.log(this.list.length)
                            this.setState({
                                searchProducts: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.list)
                            })
                        }}
                       underlineColorAndroid='white'
                       placeholderTextColor='white'
                       placeholder={Strings.search}
                       style={{color:'white', width:responsiveWidth(85),marginHorizontal:moderateScale(3),}}
                        />

                        <Icon type='EvilIcons' name='search' style={{marginHorizontal:moderateScale(3), color:'white',fontSize:responsiveFontSize(8)}} />
                    </View>

                    <View style={{elevation:20,zIndex:100,flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(100),justifyContent:'space-between',alignItems:'center',alignSelf:'center',backgroundColor:'white'}}>
                        <Text style={{marginHorizontal:moderateScale(5), fontSize:18,color:colors.darkPrimaryColor,fontWeight:'600',fontStyle:'italic'}} >{Strings.sort}</Text>
                        <View style={{marginHorizontal:moderateScale(5),}}>
                        <Picker
                            mode="dropdown"
                            iosHeader="Select your SIM"
                            iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }}/>}
                            selectedValue={this.state.sortType}
                            onValueChange={(value, index) => {
                                this.setState({sortType:value,index:index})
                                this.getProducts(1,true,types[index])
                            }}
                            style={{width:responsiveWidth(35)}}
                            >
                            <Picker.Item key='RANDOM' label={Strings.random} value='RANDOM' />
                            <Picker.Item key='HIGHTPRICE' label={Strings.highPrice} value='HIGHTPRICE' />
                            <Picker.Item key='LOWPRICE' label={Strings.lowPrice} value='LOWPRICE' />
                        </Picker>
                        </View>
                    </View>
                    <RecyclerListView            
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={this.state.flag>0 ? this.state.searchProducts :this.state.allCategories}
                    rowRenderer={this.renderRow}
                    renderFooter={this.renderFooter}
                    onEndReached={() => {
                    
                        if(this.page <= this.props.pages){
                            this.page++;
                            this.getProducts(this.page, false,types[this.state.index]);
                        }
                        
                    }}
                    refreshControl={<RefreshControl colors={["#B7ED03"]}
                        refreshing={this.state.refresh}
                        onRefresh={() => {
                        this.page = 1
                        this.getProducts(this.page, true,types[this.state.index]);
                        }
                        } />}
                    onEndReachedThreshold={.5}
                    />

            </View>
                }
                
            </View>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps,mapDispatchToProps)(CategoryProducts);
