import React, { Component } from 'react';
import {
  View,Image,AsyncStorage,StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, moderateScale } from "../utils/responsiveDimensions";
import * as Progress from 'react-native-progress';
import * as colors from '../assets/colors';
import Strings from '../assets/strings';
import {getUser,userToken} from '../actions/AuthActions';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
import firebase,{Notification } from 'react-native-firebase';
import {putLocalOrder} from '../actions/OrderAction';


class SplashScreen extends Component {

 
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    
    
    
    getOrderFromLocal =  () => {
        const {orders,ordersData} = this.props;
       // AsyncStorage.multiRemove(['orderData','order'])
       if(ordersData.length == 0){
        AsyncStorage.multiGet(['orderData','order'], (err, result) => {
            if (!err && result[1][1] != null){
                const orderDataList = JSON.parse(result[0][1])
                const orderist = JSON.parse(result[1][1])
                console.log("EXSIT ORDER")
                console.log(orderDataList)
                console.log(orderist)
                this.props.putLocalOrder(orderist,orderDataList)
              
                
            }
            else {
                console.log(result)
                console.log(" not EXSIT ORDER")
            }
          });   
         
       }

      }

    checkToken = async (token) => {
        this.props.userToken(token);
        t = await AsyncStorage.getItem('@BoodCarToken')    
        if(t){
            console.log('Exist Token')
            if(t==token){
                console.log('Token is equal')
            }else{
                console.log('New Token')
                console.log(t)
                console.log(token)
                axios.put(`${BASE_END_POINT}updateToken`, JSON.stringify({
                    oldToken: t,
                    newToken: token,
                  }), {
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${this.props.currentUser.token}`
                  },
                }).then(rsponse=>{
                    AsyncStorage.setItem('@BoodCarToken',token)
                }).catch(error=>{
                    console.log("token Error")
                    console.log(error)
                    console.log(error.response)
                })     
            }
        }else{
            console.log("save token")
            AsyncStorage.setItem('@BoodCarToken',token)
        } 

        
    }

    ServerNotification(title,body) {
        console.log("ANI CAPO   ",title,"    ",body)
        let notification = new firebase.notifications.Notification();
        notification = notification.setNotificationId(new Date().valueOf().toString())
        .setTitle( title)
        .setBody(body)
        .setSound("bell.mp3")
        .setData({
          now: new Date().toISOString()
        });
       notification.ios.badge = 10
       //notification.android.setAutoCancel(true);  
        notification.android.setBigPicture("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png", "https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg", "content title", "summary text")
        notification.android.setColor("red")
        notification.android.setColorized(true)
        notification.android.setOngoing(true)
        notification.android.setPriority(firebase.notifications.Android.Priority.High)
        notification.android.setSmallIcon("ic_launcher")
        notification.android.setVibrate([300])
        notification.android.addAction(new firebase.notifications.Android.Action("view", "ic_launcher", "VIEW"))
       // notification.android.addAction(new firebase.notifications.Android.Action("reply", "ic_launcher", "REPLY").addRemoteInput(new firebase.notifications.Android.RemoteInput("input")) )
       notification.android.setChannelId("test-channel")

        firebase.notifications().displayNotification(notification)
    
      }

      
    
    componentDidMount(){  
        this.disableDrawer();

        
        console.log('cdm')
        this.checkLanguage();
        setTimeout(() => {
            this.checkLogin();
        }, 2000);
       
        this.checkColor()

        firebase.messaging().hasPermission()
        .then(enabled => {
            if (enabled) {
            console.log(" i have permession")
            } else {
                console.log(" i not have permession")
            firebase.messaging().requestPermission()
            } 
        });     
        
        firebase.messaging().getToken().then(token => {
            console.log("TOKEN (getFCMToken)", token);
            this.checkToken(token);
          }).catch(error=>{
            console.log("TOKEN Error");         
            console.log(error);
          });

          firebase.messaging().onTokenRefresh(token => {
            axios.put(`${BASE_END_POINT}updateToken`, JSON.stringify({
                oldToken: this.props.userTokens,
                newToken: token,
              }), {
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            }).then(rsponse=>{
                AsyncStorage.setItem('@BoodCarToken',token)
            }).catch(error=>{
                console.log("token Error")
                console.log(error)
                console.log(error.response)
            })
         });


        firebase.notifications().onNotification((notification) => {
            console.log("mohamed anwer bas")
            console.log(notification)
            console.log(notification._title,notification._body)
          
            this.ServerNotification(notification._title,notification._body)
            //this.props.navigator.push({
              //  screen:'Notifications',
            // passProps: {hideMenu:true}
            //})
           /* if(notification.android.actions[0]=='Login'){
                  this.props.navigator.push({
                        screen:'Login',
                    // passProps: {hideMenu:true}
                    })
            }else{
                this.props.navigator.push({
                    screen:'Home',
                // passProps: {hideMenu:true}
                })
            }*/
        });

      
        firebase.notifications().onNotificationOpened((notificationOpen) => {
            console.log("noti open")
            console.log(notificationOpen)
            console.log("noti body   ",notificationOpen.notification)
            if(notificationOpen.action == 'android.intent.action.VIEW'){
                  this.props.navigator.push({
                   
                    screen:'Notifications',
                })
            }
        });

        
    
  

      
       /*FCM.on(FCMEvent.RefreshToken, (token) => {
            AsyncStorage.setItem('@BoodCarToken',token)
            this.props.userToken(token)
        });*/
       /* axios.put(`${BASE_END_POINT}updateToken`, JSON.stringify({
            oldToken: this.props.userTokens,
            newToken: token,
          }), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(rsponse=>{
            console.log("done done")
            AsyncStorage.setItem('@BoodCarToken',token)
            this.props.userToken(token)
        }).catch(error=>{
            console.log("token Error")
            console.log(error)
            console.log(error.response)
        })

        */

        this.getOrderFromLocal()
        
     
    }


    
    
    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    checkLogin = async () => {      
        const userJSON = await AsyncStorage.getItem('@QsathaUser');
        if(userJSON){
            const userInfo = JSON.parse(userJSON);
            if(userInfo.user.type=='CLIENT'){
                this.props.getUser(userInfo);
                console.log(this.props.currentUser)       
                this.props.navigator.resetTo({
                     screen: 'Home',         
                     animated: true,
                 });         
            }else if(userInfo.user.type=='SALES-MAN'){
                this.props.getUser(userInfo);
                console.log(this.props.currentUser)       
                this.props.navigator.resetTo({
                     screen: 'Home',         
                     animated: true,
                 });         
            }           
        }else{
            this.props.navigator.resetTo({
                screen:'Home',
                animated: true,
                //passProps: {hideMenu:true}
            })
        }
        
    }

    checkLanguage = async () => {
        console.log("lang0   "+lang)
       const lang = await AsyncStorage.getItem('@lang');
       console.log("lang   "+lang)
       if(lang==='ar'){
            this.props.changeLanguage(true);
            Strings.setLanguage('ar')
        }else{
            this.props.changeLanguage(false);
            Strings.setLanguage('en')
        }      
    }

    checkColor = async () => {
       const color = await AsyncStorage.getItem('@color');
       console.log("color  ",color)
       if(color){
        this.props.changeColor(color);
       }
       
    }

   
    render(){
        return(
            <View style={{ flex:1,justifyContent:'center',alignItems:'center'}} source={require('../assets/imgs/boodyCarBackground.png')} >
                <StatusBar backgroundColor='black'  hidden />
                <Image style={{width:responsiveWidth(80),height:responsiveHeight(40)}} source={require('../assets/imgs/mtqst.png')} />
                <View style={{marginTop:moderateScale(9)}}>
                <Progress.Bar indeterminate={true} color={colors.darkPrimaryColor} progress={0.3} width={200} />
                </View>
            </View>
        );
    }
}

const mapToStateProps = state => ({
    currentUser : state.auth.currentUser,
    userTokens: state.auth.userToken,
    orders: state.order.orders,
    ordersData: state.order.ordersData,
})

const mapDispatchToProps = {
    getUser,
    changeLanguage,
    changeColor,
    userToken,
    putLocalOrder
}

export default connect(mapToStateProps,mapDispatchToProps)(SplashScreen);

