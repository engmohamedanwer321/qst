import React,{Component} from 'react';
import {
     View,Text,TouchableOpacity, ScrollView,Alert
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppHeader from '../common/AppHeader'
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import { Button, Icon } from 'native-base';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';


class ProductDetails extends Component {
  state = {
    images: this.props.product.img,
    index:0,
    showDialog:false,
  };

  static navigatorStyle = {
    navBarHidden: true,
    //statusBarColor: colors.darkPrimaryColor
  };

  constructor(props) {
    super(props);
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      });   
    }
  componentDidUpdate(){
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      }); 
  }

  componentDidMount() {
    this.enableDrawer();
  

    console.log('FLAG FLAG ',this.props.offer)
  }

  enableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

  
  renderSlideShow = () => {
    const {isRTL} = this.props;
    
    return(
        <View style={{marginBottom:moderateScale(6),width:responsiveWidth(100),height:responsiveHeight(30),justifyContent:'center',alignItems:'center',marginTop:moderateScale(8)}}>
            <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                <TouchableOpacity
                   onPress={()=>{
                    if(this.state.index==0){
                        this.setState({index:0})
                    }else{
                        this.setState({index:this.state.index-1})
                    }
                }} 
                >
                    <Icon name={isRTL? 'chevron-right':'chevron-left'} type='Entypo' style={{color:'#ff6f61'}} />
                </TouchableOpacity >
                
                 <TouchableOpacity onPress={()=>this.setState({showDialog:true})}>
                 <FastImage resizeMode='contain' style={{width:responsiveWidth(50),height:responsiveHeight(20), marginHorizontal:moderateScale(15)}} source={{uri:this.state.images[this.state.index]}} />
                 </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>{
                    if(this.state.index==this.state.images.length-1){
                        this.setState({index:0})
                    }else{
                        this.setState({index:this.state.index+1})
                    }
                }}
                >
                    <Icon name={isRTL? 'chevron-left':'chevron-right'} type='Entypo' style={{color:'#ff6f61'}} />
                </TouchableOpacity>
            </View>

            
            <View style={{marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
               
                {this.state.images.map((img,i)=>{
                    if(i<4){
                        return(
                            <TouchableOpacity
                            onPress={()=>{
                                this.setState({index:i})
                            }}
                            style={{borderColor:this.state.index==i? '#ff6f61' : '#CCCCCC', borderWidth:1, width:responsiveWidth(18),height:responsiveHeight(8), marginHorizontal:moderateScale(2),marginVertical:moderateScale(1.5)}}>
                               <FastImage resizeMode='stretch' style={{width:responsiveWidth(18),height:responsiveHeight(8),}} source={{uri:img}} />
                           </TouchableOpacity>
                        )
                    }
                }
                )}
                
            </View>
        </View>
    )
}

renderProductDetails = (type,value,hasofer) =>{
    const {isRTL} = this.props
    return(
        <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',flexDirection:isRTL?'row-reverse':'row' , width:responsiveWidth(100),minHeight:responsiveHeight(7),justifyContent:'space-between',alignItems:'center'}}>
            <Text style={{paddingHorizontal:moderateScale(9),color:'#06134C',fontSize:responsiveFontSize(6)}}>{type}</Text>
            <Text style={{ textDecorationLine:hasofer?'line-through':'none' , marginHorizontal:moderateScale(9),color:hasofer?'red':colors.skipIconColor,fontSize:responsiveFontSize(6)}}>{value}</Text>
        </View>
    )
}
  

imageDialog = () =>(
  <Dialog
    dialogStyle={{justifyContent:'center',alignItems:'center'}}
    width={responsiveWidth(70)}
    height={responsiveHeight(40)}
    visible={this.state.showDialog}
    onTouchOutside={()=>{
      this.setState({showDialog:false})
    }}
  >       
    <FastImage resizeMode='contain' source={{uri:this.props.product.img[this.state.index]}} style={{width:responsiveWidth(68),height:responsiveHeight(38)}} />            
  </Dialog>
)
  
  
  render() {
    const {product,navigator,offer,update,barColor } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <AppHeader  title={product.name} showBack navigator={this.props.navigator} />                   
        <ScrollView style={{ flex: 1,marginBottom:moderateScale(10) }}>
        {this.renderSlideShow()}

        <View style={{alignSelf:'center',marginHorizontal:moderateScale(20),marginVertical:moderateScale(5)}}>
            <Text style={{textAlign:'center'}}> {product.description} </Text>
        </View>

        <View style={{marginVertical:moderateScale(10)}} >
            {this.renderProductDetails(Strings.productName,product.name)}
            {this.renderProductDetails(Strings.category,product.category[0].categoryname)}
            {this.renderProductDetails(Strings.firm,product.company)}   
            {this.renderProductDetails(Strings.price,product.installmentPrice,product.hasOffer)}
            {/*this.renderProductDetails(Strings.hasOffer,""+product.hasOffer)*/}
            {product.hasOffer&&this.renderProductDetails(Strings.priceAfterOffer,product.offer)}
            {product.properties.map(item=>(
              this.renderProductDetails(item.attr,item.value)
            ))}
            <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',width:responsiveWidth(100)}}></View>
        </View>
        </ScrollView>

        {this.imageDialog()}

        {!this.props.hide&&
        <TouchableOpacity
        onPress={()=>{
            navigator.push({
                screen: 'MoreProductDetails',
                passProps: {
                    product: product,
                    offer:offer?true:false
                }
            })
        }} 
        style={{backgroundColor:barColor, position:'absolute',left:0,right:0,bottom:0, width:responsiveWidth(100),height:responsiveHeight(8), justifyContent:'center', alignItems:'center'}} >
            <Text style={{color:'white',fontSize:responsiveFontSize(7)}} > {update?Strings.update: Strings.buying} </Text>
        </TouchableOpacity>
        }
      </View>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
})

const mapDispatchToProps = {
  
}

export default connect(mapToStateProps,mapDispatchToProps)(ProductDetails);