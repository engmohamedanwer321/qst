import React,{Component} from 'react';
import {View,NetInfo,FlatList,Text,TouchableOpacity} from 'react-native';
import { responsiveWidth, responsiveHeight,moderateScale,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import { TabView, TabBar } from 'react-native-tab-view';
import LottieView from 'lottie-react-native';
import OrderCard from '../components/OrderCard';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import {Thumbnail} from 'native-base';
import { BASE_END_POINT} from '../AppConfig';
import moment from 'moment'
import {removeItem} from '../actions/MenuActions';


class MyOrders extends Component {

    state= {
        showDialog:false,
        errorText:null,
        acceptedOrders: [],
        acceptedOrdersLoading:true,
        pendingOrders: [],
        pendingOrdersLoading:true,
        index: 0,
        routes: [
        { key: 'MyAcceptedTab', title:Strings.accepted},
        { key: 'MyPendingTab', title:Strings.pending },
        ], 
    }

    static navigatorStyle = {
        navBarHidden: true,
        //statusBarColor: colors.darkPrimaryColor,
    };
    componentDidUpdate(){
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
      }

    constructor(props) {
        super(props);    
        this.props.navigator.setStyle({
            statusBarColor: this.props.barColor,
            
          }); 
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getPendingOrders()
                this.getAcceptedOrders()
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getPendingOrders()
                    this.getAcceptedOrders()
                }
            }
          );      
    }

    getPendingOrders = () => {
        axios.get(`${BASE_END_POINT}orders?clientId=${this.props.currentUser.user.id}&status=PENDING`)
                .then(response => {
                    this.setState({pendingOrdersLoading:false, pendingOrders:response.data.data})
                    console.log("Pending Orders")
                    console.log(response.data)
                    /*if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }*/
                }).catch(error => {
                    this.setState({pendingOrdersLoading:false})
                    console.log(error.response);
                    console.log(error);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
    }

    getAcceptedOrders = () => {
        axios.get(`${BASE_END_POINT}premiums?client=${this.props.currentUser.user.id}&&status=PENDING`)
                .then(response => {
                    this.setState({acceptedOrdersLoading:false, acceptedOrders:response.data.data})
                    console.log("accepted Orders")
                    console.log(response.data)
                    /*if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }*/
                }).catch(error => {
                    this.setState({acceptedOrdersLoading:false})
                    console.log(error.response);
                    console.log(error);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

   
    renderAcceptedOrders = () => {
        const {navigator,isRTL,barColor} = this.props
        return(
            <FlatList
        data={this.state.acceptedOrders}
        renderItem={({item})=> (
            <View style={{elevation:10,marginVertical:moderateScale(2), alignSelf:'center', backgroundColor:'white', width:responsiveWidth(97)}}>
            <View style={{alignItems:'center', margin:moderateScale(5),flexDirection:isRTL?'row-reverse':'row'}}>
                <Thumbnail small source={require('../assets/imgs/mtqst.png')} />
                <Text style={{marginHorizontal:moderateScale(3)}} >{Strings.qsatha}</Text>
            </View>

            
                <TouchableOpacity
                onPress={()=>{
                    navigator.push({
                        screen: 'BillDetails',
                        animated:true,
                        passProps:{
                            product:item,
                            flag:true
                        }
                    })
                   
                }}
                 style={{backgroundColor:'#f9f5f5', alignSelf:'center', marginVertical:moderateScale(1),alignItems:'center',width:responsiveWidth(93),height:responsiveHeight(10)}}>              
            
            <View style={{flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(93),alignItems:'center',flex:1}}>
               
                <View style={{ flex:1,alignItems:'center'}} >
                <Text style={{color:colors.darkPrimaryColor, fontStyle:'italic',fontSize:responsiveFontSize(6)}} >
                   {Strings.name}
                </Text>
                </View>
               
                <View style={{ flex:1,alignItems:'center'}} >
                <Text style={{color:colors.darkPrimaryColor, fontStyle:'italic',fontSize:responsiveFontSize(6)}}>
                    {Strings.months}
                </Text>
                </View>

                <View style={{ flex:1,alignItems:'center'}} >
                <Text style={{color:colors.darkPrimaryColor, fontStyle:'italic',fontSize:responsiveFontSize(6)}}>
                    {Strings.perMonth}
                </Text>
                </View>
            </View>

            <View style={{flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(93),alignItems:'center',marginHorizontal:moderateScale(1),flex:1}}>
               
                <View style={{ flex:1,alignItems:'center'}} >
                <Text style={{fontStyle:'italic'}}>
                  {item.product.name}
                </Text>
                </View>
                

                <View style={{ flex:1,alignItems:'center'}} >
                <Text style={{fontStyle:'italic'}}>
                {item.totalMonth}
                </Text>
                </View>

                <View style={{ flex:1,alignItems:'center'}} >
                <Text style={{fontStyle:'italic'}}>
                {item.costPerMonth}
                </Text>
                </View>
            </View>    
            </TouchableOpacity>

            <TouchableOpacity
            onPress={()=>{
                navigator.push({
                    screen: 'ProductDetails',
                    passProps:{
                        product:item.product,
                        hide:true,
                    }
                })
            }}
             style={{backgroundColor:barColor, alignSelf:'center',marginTop:moderateScale(4),justifyContent:'center',alignItems:'center',width:responsiveWidth(97),height:responsiveHeight(6)}}>
                <Text style={{fontsize:responsiveFontSize(2.6), color:'white'}}>{Strings.productDetails}</Text>
            </TouchableOpacity>

            <View style={{margin:moderateScale(7),alignSelf:isRTL?'flex-start':'flex-end'}} >
                <Text>{moment(item.createdAt).fromNow()}</Text>
            </View>
        
        </View>
        )}
       />
        )
    }

    renderPendingOrders = () => (
       <FlatList
       data={this.state.pendingOrders}
       renderItem={({item})=> <OrderCard navigator={this.props.navigator} data={item} />}
      />
    )

    _renderTabBar = props => 
    <TabBar
     {...props} 
      style={{backgroundColor:this.props.barColor}} />

      MyAcceptedTab = () => {
        //const {currentUser,isRTL,orders} = this.props; 
         return (
            <View style={{ flex:1}}>
            {this.state.acceptedOrdersLoading?
           <View style={{flex:1}}>
               <View style={{ flex:1}}> 
               <LottieView
                 style={{margin:0,padding:0, width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                   source={require('../assets/animations/smartGarbageLoading.json')}
                   autoPlay
                   loop
                   />
               </View>

               <View style={{ flex:1}}> 
               <LottieView
               style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                   source={require('../assets/animations/smartGarbageLoading.json')}
                   autoPlay
                   loop
                   />
               </View>

               <View style={{ flex:1}}> 
               <LottieView
               style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                   source={require('../assets/animations/smartGarbageLoading.json')}
                   autoPlay
                   loop
                   />
               </View>
               

           </View>
           :
           this.renderAcceptedOrders()
           }
       </View>
         );
     }

     MyPendingTab = () => {
        //const {isRTL}=this.props;
        return(     
            <View style={{ flex:1}}>
                 {this.state.pendingOrdersLoading?
                <View style={{flex:1}}>
                    <View style={{ flex:1}}> 
                    <LottieView
                      style={{margin:0,padding:0, width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>

                    <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>

                    <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>
                    

                </View>
                :
                this.renderPendingOrders()
                }
            </View>
        )
    }

   

    render(){
        const {navigator} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.myOrders} showBurger navigator={navigator}/>
                <TabView      
                    style={{elevation:4}}
                    navigationState={this.state}
                    renderTabBar={this._renderTabBar}
                    renderScene={renderScene = ({ route }) => {
                        switch (route.key) {
                          case 'MyAcceptedTab':
                            return <this.MyAcceptedTab />
                          case 'MyPendingTab':
                            return <this.MyPendingTab/>
                          default:
                            return null;
                        }
                      }}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{height:responsiveHeight(6), width: responsiveWidth(100)}}
                />

                

            </View>    
        );
    }
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser:state.auth.currentUser,
    barColor: state.lang.color 
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(MyOrders);
