import React, { Component } from 'react';
import {
  View,TouchableOpacity,Keyboard,ScrollView,Image,TextInput
} from 'react-native';
import { connect } from 'react-redux';
import { Button,Item, Picker, Label,Icon,Input} from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import Strings from  '../assets/strings';
import { Field, reduxForm } from "redux-form"
import Checkbox from 'react-native-custom-checkbox';
import { signup } from '../actions/AuthActions';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {removeItem} from '../actions/MenuActions';

import withPreventDoubleClick from '../components/withPreventDoubleClick';


const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);



const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } else if (!values.phoneNumber.startsWith('01')) {
        errors.phoneNumber = Strings.errorStartPhone;
    }  else if (values.phoneNumber.length < 11 || values.phoneNumber.length > 11) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }

    if (!values.password) {
        errors.password = Strings.require;
    }

    if (!values.cardNumber) {
      errors.cardNumber = Strings.require;
    }else if (values.cardNumber.length<14 || values.cardNumber.length>14 ) {
      errors.cardNumber = Strings.cardNumFormat;
    }

  if (!values.address) {
    errors.address = Strings.require;
  }
   

    if (!values.confirmPassword) {
        errors.confirmPassword = Strings.require;
    } else if (values.password != values.confirmPassword) {
        errors.confirmPassword = Strings.errorConfirmPassword;
    }
    
    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class Signup extends Component {
  state = {
    agreeTerms: false,
    type: 'WORKER',
    selectedCity: 'cairo',
    otheMobilePhone: null
  };

  static navigatorStyle = {
    navBarHidden: true,
    //statusBarColor: colors.darkPrimaryColor
  };

  componentDidUpdate(){
    this.props.navigator.setStyle({
        statusBarColor: this.props.barColor,
        
      }); 
  }

  constructor(props) {
    super(props);
    rootNavigator = this.props.navigator;
    this.props.navigator.setStyle({
      statusBarColor: this.props.barColor,
      
    }); 
  }

  componentDidMount() {
    this.disableDrawer();
  }


  disableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

  onSignup(values) {
    console.log(values)
    console.log(values.length)
      let phones=[]
      phones.push(values.phoneNumber)
      if(this.state.otheMobilePhone){
        phones.push(this.state.otheMobilePhone)
      }

      console.log('phone')
    console.log(phones)

      const user = {
        firstname:values.firstName,
        lastname:values.lastName ,
        cardNum: values.cardNumber ,
        address: values.address,
        email: values.email.toLowerCase(),
        phone: phones,
        password: values.password,
        type: 'CLIENT',
        area: this.state.selectedCity
      }
       this.props.signup(user,this.props.navigator)
     
  }

  renderSignupButtons() {
    const { handleSubmit,barColor } = this.props;
    return (
      <MyButton
        onPress={handleSubmit(this.onSignup.bind(this))}
        style={{ alignSelf: "center",marginVertical: moderateScale(10),justifyContent: "center",alignItems: "center",height: responsiveHeight(8),width: responsiveWidth(50),backgroundColor: barColor,borderRadius: moderateScale(10)}}
      >
        <AppText fontSize={responsiveFontSize(7)} text={Strings.signup} color="white"/>
      </MyButton>
    );
  }

  renderTerms = () => {
    return (
      <View
        style={{justifyContent: "center",alignItems: "center",marginTop: moderateScale(5),width: responsiveWidth(80),flexDirection: this.props.isRTL ? "row-reverse" : "row",marginHorizontal: moderateScale(2)}}
      >
        <View
          style={{marginRight: 3,alignSelf: this.props.isRTL ? "flex-end" : "flex-start"}}
        >
          <Checkbox
            checked={this.state.agreeTerms}
            style={{backgroundColor: "white",color: colors.primaryColor,borderRadius: 5}}
            onChange={(name, checked) => {
              this.setState({ agreeTerms: checked });
            }}
          />
        </View>
        <View
          style={{ alignSelf: this.props.isRTL ? "flex-end" : "flex-start" }}
        >
          <AppText
            text={Strings.term1}
            color="gray"
            fontSize={responsiveFontSize(6)}
          />
        </View>
        <MyTouchableOpacity
          onPress={() => {
            this.props.navigator.push({
              screen: "TermsAndCondictions",
              animated: true
            });
          }}
          style={{
            borderBottomWidth: 1,
            borderBottomColor: colors.buttonColor,
            alignSelf: this.props.isRTL ? "flex-end" : "flex-start"
          }}
        >
          <AppText
            text={Strings.term2}
            fontSize={responsiveFontSize(6)}
            color={colors.buttonColor}
          />
        </MyTouchableOpacity>
      </View>
    );
  };

  renderCitesPicker = () => {
    console.log(this.state.companies);
    return(
        <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(80),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row',borderColor:'transparent'}}
        >
        <View style={{ flex:1}}>
         <Label style={{fontSize:responsiveFontSize(6.5),color:'gray',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.city}</Label>
        </View>
         <View style={{flex:1}}>
             {
                 this.state.companyLoad?
                 <AppSpinner isRTL={this.props.isRTL}/>
                 :
                 <Picker           
                 mode="dropdown"
                 iosHeader="Select your Company"
                 iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                 selectedValue={this.state.selectedCity}
                 style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                 onValueChange={(value,index)=>{
                    this.setState({selectedCity:value});
                 }}
                 >
                 <Picker.Item key='cairo' label={Strings.cairo} value='cairo'/>
                 <Picker.Item key='ismailia' label={Strings.ismailia} value='ismailia'/>
                 <Picker.Item key='alexandria' label={Strings.alexandria} value='alexandria'/>
                 <Picker.Item key='giza' label={Strings.giza} value='giza'/>
                 <Picker.Item key='aswan' label={Strings.aswan} value='aswan'/>
                 <Picker.Item key='laxour' label={Strings.laxour} value='laxour'/>
                 <Picker.Item key='asyut' label={Strings.asyut} value='asyut'/>
                 <Picker.Item key='beheira' label={Strings.beheira} value='beheira'/>
                 <Picker.Item key='mansoura' label={Strings.mansoura} value='mansoura'/>
                 <Picker.Item key='damietta' label={Strings.damietta} value='damietta'/>
                 <Picker.Item key='faiyum' label={Strings.faiyum} value='faiyum'/>
                 <Picker.Item key='tanta' label={Strings.tanta} value='tanta'/>
                 <Picker.Item key='kafrElSheikh' label={Strings.kafrElSheikh} value='kafrElSheikh'/>
                 <Picker.Item key='minya' label={Strings.minya} value='minya'/>
                 <Picker.Item key='shibinElKom' label={Strings.shibinElKom} value='shibinElKom'/>
                 <Picker.Item key='arish' label={Strings.arish} value='arish'/>
                 <Picker.Item key='portSaid' label={Strings.portSaid} value='portSaid'/>
                 <Picker.Item key='banha' label={Strings.banha} value='banha'/>
                 <Picker.Item key='qena' label={Strings.qena} value='qena'/>
                 <Picker.Item key='zagazig' label={Strings.zagazig} value='zagazig'/>
                 <Picker.Item key='suez' label={Strings.suez} value='suez'/>
                 <Picker.Item key='sohag' label={Strings.sohag} value='sohag'/>
                 <Picker.Item key='elTor' label={Strings.elTor} value='elTor'/>

                
             </Picker>
             }
         </View>
     </Item>
    )
}

  
  renderContent() {
    const { isRTL } = this.props;
    return (
      <View>
        <View
          style={{
            width: responsiveWidth(80),
            flexDirection: isRTL ? "row-reverse" : "row"
          }}
        >
          <View style={{ width: responsiveWidth(39) }}>
            <Field
              borderColor="gray"
              style={{ width: responsiveHeight(80) }}
              textColor={colors.primaryColor}
              name="firstName"
              isRTL={this.props.isRTL}
              marginBottom={moderateScale(3)}
              label={Strings.firstName}
              component={InputComponent}
              returnKeyType="done"
              onSubmit={() => {
                Keyboard.dismiss();
              }}
            />
          </View>

          <View
            style={{
              marginHorizontal: moderateScale(2),
              width: responsiveWidth(39)
            }}
          >
            <Field
              borderColor="gray"
              style={{ width: responsiveHeight(80) }}
              textColor={colors.primaryColor}
              name="lastName"
              isRTL={this.props.isRTL}
              marginBottom={moderateScale(3)}
              label={Strings.secondName}
              component={InputComponent}
              returnKeyType="done"
              onSubmit={() => {
                Keyboard.dismiss();
              }}
            />
          </View>
        </View>

        <Field
          borderColor="gray"
          style={{ width: responsiveHeight(80) }}
          textColor={colors.primaryColor}
          numeric
          name="cardNumber"
          isRTL={this.props.isRTL}
          marginBottom={moderateScale(3)}
          label={Strings.cardNumber}
          component={InputComponent}
          returnKeyType="done"
          onSubmit={() => {
            Keyboard.dismiss();
          }}
        />

        <Field
          borderColor="gray"
          style={{ width: responsiveHeight(80) }}
          textColor={colors.primaryColor}
          numeric
          name="phoneNumber"
          isRTL={this.props.isRTL}
          marginBottom={moderateScale(3)}
          label={Strings.mobileNumber}
          component={InputComponent}
          returnKeyType="done"
          onSubmit={() => {
            Keyboard.dismiss();
          }}
        />

        <Item style={{marginVertical:moderateScale(5), borderBottomColor:'gray', alignSelf:'center', width: responsiveWidth(80),}} floatingLabel>
            <Label style={{fontSize:13}} >{Strings.otherMobileNumber}</Label>
            <Input keyboardType='phone-pad' style={{textAlign:this.props.isRTL?'right':'left', direction:this.props.isRTL?'rtl':'ltr'}}
              onChangeText={(val)=>{
              this.setState({otheMobilePhone:val})
              console.log('omp  ',val)
            }}
            />
        </Item>

        

        {/*<Field
          
          borderColor="gray"
          style={{ width: responsiveHeight(80) }}
          textColor={colors.primaryColor}
          numeric
          name="otherPhoneNumber"
          isRTL={this.props.isRTL}
          marginBottom={moderateScale(3)}
          label={Strings.otherMobileNumber}
          component={InputComponent}
          returnKeyType="done"
          onSubmit={() => {
            Keyboard.dismiss();
          }}
        />*/}

        <Field
          borderColor="gray"
          style={{ width: responsiveHeight(80) }}
          textColor={colors.primaryColor}
          name="address"
          isRTL={this.props.isRTL}
          marginBottom={moderateScale(3)}
          label={Strings.address}
          component={InputComponent}
          returnKeyType="done"
          onSubmit={() => {
            Keyboard.dismiss();
          }}
        />

        {this.renderCitesPicker()}

        <Field
          borderColor="gray"
          style={{ width: responsiveHeight(80) }}
          textColor={colors.primaryColor}
          email
          name="email"
          isRTL={this.props.isRTL}
          marginBottom={moderateScale(3)}
          label={Strings.email}
          component={InputComponent}
          returnKeyType="done"
          onSubmit={() => {
            Keyboard.dismiss();
          }}
        />

        <Field
          borderColor="gray"
          style={{ width: responsiveHeight(80) }}
          textColor={colors.primaryColor}
          password
          name="password"
          isRTL={this.props.isRTL}
          marginBottom={moderateScale(3)}
          label={Strings.password}
          component={InputComponent}
          returnKeyType="done"
          onSubmit={() => {
            Keyboard.dismiss();
          }}
        />

        <Field
          borderColor="gray"
          style={{ width: responsiveHeight(80) }}
          textColor={colors.primaryColor}
          password
          name="confirmPassword"
          isRTL={this.props.isRTL}
          marginBottom={moderateScale(3)}
          label={Strings.confirmPassword}
          component={InputComponent}
          returnKeyType="done"
          onSubmit={() => {
            Keyboard.dismiss();
          }}
        />

        {/*this.renderTerms()*/}
        {this.renderSignupButtons()}
      </View>
    );
  }


  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        
        <Image style={{alignSelf:'center',marginTop:moderateScale(15), width:responsiveWidth(80),height:responsiveHeight(25)}} source={require('../assets/imgs/mtqst.png')} />

        <View
          style={{
            marginTop: moderateScale(8),
            width: responsiveWidth(80),
            alignSelf: "center"
          }}
        >
          {this.renderContent()}
        </View>

        {this.props.loading && (
          <LoadingDialogOverlay title={Strings.checkSignup} />
        )}

       
      </ScrollView>
    );
  }
}

const form = reduxForm({
    form: "Signup",
    validate,
})(Signup)

const mapDispatchToProps = {
    signup,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

