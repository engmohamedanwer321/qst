package com.mtqst;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.airbnb.android.react.maps.MapsPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.reactnativenavigation.NavigationApplication;
import com.rncloudinary.RNCloudinaryPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
//import com.reactnativenavigation.NavigationReactPackage;
import org.wonday.pdf.RCTPdfView;
import com.horcrux.svg.SvgPackage;
import ui.toasty.RNToastyPackage;
import com.oblador.vectoricons.VectorIconsPackage;

import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.reactnativenavigation.bridge.NavigationReactPackage;

//
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
//

import java.util.Arrays;
import java.util.List;

/*
 new MainReactPackage(),
            new RNFetchBlobPackage(),
            new MapsPackage(),
            new RNFirebasePackage(),
            new LottiePackage(),
            new RNCloudinaryPackage(),
            new FastImageViewPackage(),
            new RNFusedLocationPackage(),
            new RNGestureHandlerPackage(),
            new PickerPackage(),
            new ReactNativeLocalizationPackage(),
            new NavigationReactPackage(),
            new RCTPdfView(),
            new SvgPackage(),
            new RNToastyPackage(),
            new VectorIconsPackage(),
              new RNFirebaseMessagingPackage(),
              new RNFirebaseNotificationsPackage()
 */


public class MainApplication extends NavigationApplication {

  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }

  protected List<ReactPackage> getPackages() {
    // Add additional packages you require here
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList( // <== this
            new MainReactPackage(),
            new RNFetchBlobPackage(),
            new MapsPackage(),
            new RNFirebasePackage(),
            new LottiePackage(),
            new RNCloudinaryPackage(),
            new FastImageViewPackage(),
            new RNFusedLocationPackage(),
            new RNGestureHandlerPackage(),
            new PickerPackage(),
            new ReactNativeLocalizationPackage(),
            new NavigationReactPackage(),
            new RCTPdfView(),
            new SvgPackage(),
            new RNToastyPackage(),
            new VectorIconsPackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage()
    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }
  @Override
  public String getJSMainModuleName() {
    return "index";
  }
}