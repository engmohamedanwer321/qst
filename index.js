/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
*/

import {AppRegistry,YellowBox,I18nManager} from 'react-native';
import firebase from 'react-native-firebase';
import {name as appName} from './app.json';
import App from './src/App';
I18nManager.allowRTL(false);
handler = () => message => {
    console.log("BACKGROUND MESSAGE")
}
actionHandler = () => message => {
    console.log("BACKGROUND Actions")
}
//يييي555
//new update
YellowBox.ignoreWarnings(['Warning: ...']);
console.disableYellowBox = true;
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', handler);
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundNotificationAction', actionHandler);
AppRegistry.registerComponent(appName, () => App);
